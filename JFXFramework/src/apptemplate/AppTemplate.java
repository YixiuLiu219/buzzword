package apptemplate;

import components.AppComponentsBuilder;
import components.AppDataComponent;
import components.AppFileComponent;
import components.AppWorkspaceComponent;
import javafx.application.Application;
import javafx.stage.Stage;
import propertymanager.PropertyManager;
import settings.InitializationParameters;
import ui.AppGUI;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;
import view.HelpDialogSingleton;
import view.ProfileDialogSingleton;
import view.ProfileViewSingleton;
import xmlutils.InvalidXMLFileFormatException;

import java.io.File;
import java.net.URL;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.*;

/**
 * @author Richard McKenna, Ritwik Banerjee
 */
public abstract class AppTemplate<D, F> extends Application {

    private final PropertyManager propertyManager = PropertyManager.getManager();
    private D      dataComponent; // to manage the app's data
    private F      fileComponent; // to manage the app's file I/O
    private AppGUI                gui;

    public abstract AppComponentsBuilder<D, F> makeAppBuilderHook();

    public D getDataComponent() {
        return dataComponent;
    }

    public F getFileComponent() {
        return fileComponent;
    }

    public AppGUI getGUI() {
        return gui;
    }

    @SuppressWarnings("unused")
    public String getControllerClass() {
        return "DefaultAppController";
    }

    @Override
    public void start(Stage primaryStage) {
        initializeDialogSingletons(primaryStage);
        try {
            if (loadProperties(APP_PROPERTIES_XML) && loadProperties(WORKSPACE_PROPERTIES_XML)) {
                AppComponentsBuilder<D, F> builder = makeAppBuilderHook();

                fileComponent = builder.buildFileComponent();
                dataComponent = builder.buildDataComponent();
                gui = (propertyManager.hasProperty(APP_WINDOW_WIDTH) && propertyManager.hasProperty(APP_WINDOW_HEIGHT))
                      ? new AppGUI(primaryStage, propertyManager.getPropertyValue(APP_TITLE.toString()), this,
                                   Integer.parseInt(propertyManager.getPropertyValue(APP_WINDOW_WIDTH)),
                                   Integer.parseInt(propertyManager.getPropertyValue(APP_WINDOW_HEIGHT)))
                      : new AppGUI(primaryStage, propertyManager.getPropertyValue(APP_TITLE.toString()), this);
                initStylesheet();
                gui.initStyle();
                builder.buildStylizerComponent().initStyle(gui);
            }
        } catch (Exception e) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(propertyManager.getPropertyValue(PROPERTIES_LOAD_ERROR_TITLE.toString()),
                        propertyManager.getPropertyValue(PROPERTIES_LOAD_ERROR_MESSAGE.toString()));
        }
    }

    private void initializeDialogSingletons(Stage primaryStage){
        AppMessageDialogSingleton.getSingleton().init(primaryStage);
        YesNoCancelDialogSingleton.getSingleton().init(primaryStage);
        ProfileDialogSingleton.getSingleton().init(primaryStage);
        HelpDialogSingleton.getSingleton().init(primaryStage);
        ProfileViewSingleton.getSingleton().init(primaryStage);
    }

    public boolean loadProperties(InitializationParameters propertyParameter) {
        try {
            propertyManager.loadProperties(AppTemplate.class, propertyParameter.getParameter(), PROPERTIES_SCHEMA_XSD.getParameter());
        } catch (InvalidXMLFileFormatException e) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(propertyManager.getPropertyValue(PROPERTIES_LOAD_ERROR_TITLE.toString()),
                        propertyManager.getPropertyValue(PROPERTIES_LOAD_ERROR_MESSAGE.toString()));
            return false;
        }

        return true;
    }

    public void initStylesheet() {
        URL cssResource = getClass().getClassLoader().getResource(propertyManager.getPropertyValue(APP_PATH_CSS) +
                                                                  File.separator +
                                                                  propertyManager.getPropertyValue(APP_CSS));
        assert cssResource != null;
        gui.getPrimaryScene().getStylesheets().add(cssResource.toExternalForm());
    }
}
