package controller;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Ritwik Banerjee
 */
public interface AppController {

    void handleNewRequest();

    void handleSaveRequest() throws IOException;

    void handleLoadRequest() throws IOException;

    void handleExitRequest();

    void handleProfileCreate();

    void handleProfileView();

    void handleLogin();

    void handleLogout();

    void handleModeMenu();

    void handleHelp();

    void handleClose();

    void handleStart();

    void handleHome();

    ArrayList<String> getModeList();
}
