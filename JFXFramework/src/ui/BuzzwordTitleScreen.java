package ui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

/**
 * Created by Yixiu Liu on 11/10/2016.
 */
public class BuzzwordTitleScreen extends VBox{
    private final String[] title = {"B", "U", "Z", "Z", "W", "O", "R", "D"};

    public BuzzwordTitleScreen(){
        init();
    }

    private void init(){
        VBox levelBox = new VBox();
        Text titleText = new Text("BUZZWORD");


        DropShadow ds = new DropShadow();
        Color highLight = Color.BLUE;
        double highLightRadius = 50;
        ds.setColor(highLight);
        ds.setRadius(highLightRadius);
        ds.setBlurType(BlurType.GAUSSIAN);

        FlowPane fp = new FlowPane();
        fp.setPrefWrapLength(250);
        int counter =0;
        for(int i=0; i<2; i++){
            for(int j=0; j<4; j++){

                StackPane sp = new StackPane();
                Text text = new Text(title[counter]);

                double nodeRadius = 30;
                Circle circle = new Circle(nodeRadius);

                circle.setStyle("-fx-fill: rgba(50, 50, 50, 0.5);");
                text.setStyle("-fx-fill:WHITE; -fx-font-size:20;");

                sp.getChildren().addAll(text, circle);
                text.toFront();
                text.setDisable(true);

                fp.getChildren().add(sp);
                counter++;
            }
        }

        titleText.setStyle(
                "-fx-font-size:60; " +
                        "-fx-font-family: Arial; " +
                        "-fx-fill: rgba(0, 100, 100, 0.5);" +
                        "-fx-fill: linear-gradient(to right, rgba(0, 100, 100, 0.2), rgba(0, 100, 100, 1));"
        );

        fp.setHgap(50);
        fp.setVgap(50);
        fp.setPrefWrapLength(100);

        levelBox.getChildren().addAll(fp);
        levelBox.setStyle("-fx-spacing: 20px;-fx-padding:250;");
        levelBox.setAlignment(Pos.CENTER);

        this.getChildren().addAll(titleText, levelBox);
        this.setAlignment(Pos.TOP_CENTER);
        this.setPadding(new Insets(0,0,0,0));
        //this.setStyle("-fx-spacing: 150px;");
    }
}
