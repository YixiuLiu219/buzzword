package ui;

import apptemplate.AppTemplate;
import components.AppStyleArbiter;
import controller.AppController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import propertymanager.PropertyManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_IMAGEDIR_PATH;

/**
 * This class provides the basic user interface for this application, including all the file controls, but it does not
 * include the workspace, which should be customizable and application dependent.
 *
 * @author Richard McKenna, Ritwik Banerjee
 */
public class AppGUI implements AppStyleArbiter {
    protected Stage          primaryStage;     // the application window
    protected Scene          primaryScene;     // the scene graph
    protected String         applicationTitle; // the application title

    protected AppController  appController;   // to react to file-related controls

    protected BorderPane     appPane;          // the root node in the scene graph, to organize the containers
    protected Pane           toolbarPane;      // the left toolbar
    protected Pane           rightToolbarPane; // the right toolbar
    protected Pane           titleScreen;

    /*
    protected Button         newButton;        // button to create a new instance of the application
    protected Button         saveButton;       // button to save progress on application
    protected Button         loadButton;       // button to load a saved game from (json) file
    protected Button         exitButton;       // button to exit application
    */

    protected Button         profileCreateButton;
    protected Button         profileViewButton;
    protected Button         loginButton;
    protected Button         logoutButton;
    protected Button         helpButton;
    protected Button         closeButton;
    protected Button         startButton;
    protected Button         homeButton;
    protected ComboBox<String>         modeMenuButton;

    private final double TOOL_BAR_PAD = 20;

    public final ArrayList<Node> NOT_LOGGED_HOME_SET = new ArrayList<>();
    public final ArrayList<Node> LOGGED_HOME_SET = new ArrayList<>();
    public final ArrayList<Node> STARTED_SET = new ArrayList<>();

    private int appSpecificWindowWidth;  // optional parameter for window width that can be set by the application
    private int appSpecificWindowHeight; // optional parameter for window height that can be set by the application
    
    /**
     * This constructor initializes the file toolbar for use.
     *
     * @param initPrimaryStage The window for this application.
     * @param initAppTitle     The title of this application, which
     *                         will appear in the window bar.
     * @param app              The app within this gui is used.
     */
    public AppGUI(Stage initPrimaryStage, String initAppTitle, AppTemplate app) throws IOException, InstantiationException {
        this(initPrimaryStage, initAppTitle, app, -1, -1);
    }

    public AppGUI(Stage primaryStage, String applicationTitle, AppTemplate appTemplate, int appSpecificWindowWidth, int appSpecificWindowHeight) throws IOException, InstantiationException {
        this.appSpecificWindowWidth = appSpecificWindowWidth;
        this.appSpecificWindowHeight = appSpecificWindowHeight;
        this.primaryStage = primaryStage;
        this.applicationTitle = applicationTitle;
        initializeToolbar();                    // initialize the top toolbar
        initializeToolbarHandlers(appTemplate); // set the toolbar button handlers
        initializeWindow();                     // start the app window (without the application-specific workspace)
        initializeGroupings();
        setButtons(NOT_LOGGED_HOME_SET);


        titleScreen = new BuzzwordTitleScreen();
        loadTitleScreen();
    }

    public BorderPane getAppPane() { return appPane; }
    public Pane getToolbarPane() { return toolbarPane; }
    public Pane getRightToolbarPane() { return rightToolbarPane; }
    public void loadTitleScreen(){ appPane.setCenter(titleScreen); }
    
    /**
     * Accessor method for getting this application's primary stage's,
     * scene.
     *
     * @return This application's window's scene.
     */
    public Scene getPrimaryScene() { return primaryScene; }
    
    /**
     * Accessor method for getting this application's window,
     * which is the primary stage within which the full GUI will be placed.
     *
     * @return This application's primary stage (i.e. window).
     */
    public Stage getWindow() { return primaryStage; }

    /****************************************************************************/
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR AppGUI */
    /****************************************************************************/
    
    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initializeToolbar() throws IOException {
        toolbarPane = new VBox();
        toolbarPane.setMaxWidth(222);
        toolbarPane.setMinWidth(222);
        rightToolbarPane = new VBox();
        /*
        newButton = initializeChildButton(toolbarPane, NEW_ICON.toString(), NEW_TOOLTIP.toString(), false);
        loadButton = initializeChildButton(toolbarPane, LOAD_ICON.toString(), LOAD_TOOLTIP.toString(), false);
        saveButton = initializeChildButton(toolbarPane, SAVE_ICON.toString(), SAVE_TOOLTIP.toString(), false);
        exitButton = initializeChildButton(toolbarPane, EXIT_ICON.toString(), EXIT_TOOLTIP.toString(), false);
        */

        /*
        profileCreateButton = initializeChildButton(toolbarPane, PROFILE_CREATE_ICON.toString(), PROFILE_CREATE_TOOLTIP.toString(), false);
        profileViewButton = initializeChildButton(toolbarPane, PROFILE_VIEW_ICON.toString(), PROFILE_VIEW_TOOLTIP.toString(), false);
        loginButton = initializeChildButton(toolbarPane, LOGIN_ICON.toString(), LOGIN_TOOLTIP.toString(), false);
        logoutButton = initializeChildButton(toolbarPane, LOGOUT_ICON.toString(), LOGOUT_TOOLTIP.toString(), false);
        modeMenuButton = initializeChildButton(toolbarPane, MODE_MENU_ICON.toString(), MODE_MENU_TOOLTIP.toString(), false);
        helpButton = initializeChildButton(toolbarPane, HELP_ICON.toString(), HELP_TOOLTIP.toString(), false);
        closeButton = initializeChildButton(toolbarPane, CLOSE_ICON.toString(), CLOSE_TOOLTIP.toString(), false);
        startButton = initializeChildButton(toolbarPane, START_ICON.toString(), START_TOOLTIP.toString(), false);
        homeButton = initializeChildButton(toolbarPane, HOME_ICON.toString(), HOME_TOOLTIP.toString(), false);
        */
        profileCreateButton = initializeChildButton(toolbarPane, PROFILE_CREATE_ICON.toString(), PROFILE_CREATE_TOOLTIP.toString(), PROFILE_CREATE_TEXT.toString(), false);
        profileViewButton = initializeChildButton(toolbarPane, PROFILE_VIEW_ICON.toString(), PROFILE_VIEW_TOOLTIP.toString(), PROFILE_VIEW_TEXT.toString(), false);
        loginButton = initializeChildButton(toolbarPane, LOGIN_ICON.toString(), LOGIN_TOOLTIP.toString(), LOGIN_TEXT.toString(), false);
        logoutButton = initializeChildButton(toolbarPane, LOGOUT_ICON.toString(), LOGOUT_TOOLTIP.toString(), LOGOUT_TEXT.toString(), false);
        helpButton = initializeChildButton(toolbarPane, HELP_ICON.toString(), HELP_TOOLTIP.toString(), HELP_TEXT.toString(), false);
        startButton = initializeChildButton(toolbarPane, START_ICON.toString(), START_TOOLTIP.toString(), START_TEXT.toString(), false);
        homeButton = initializeChildButton(toolbarPane, HOME_ICON.toString(), HOME_TOOLTIP.toString(), HOME_TEXT.toString(), false);

        closeButton = initializeChildButton(rightToolbarPane, CLOSE_ICON.toString(), CLOSE_TOOLTIP.toString(), false);

        //NEED TO ORGANIZE THIS LATER


        //modeMenuButton = initializeChildButton(toolbarPane, MODE_MENU_ICON.toString(), MODE_MENU_TOOLTIP.toString(), MODE_MENU_TEXT.toString(), false);
        modeMenuButton = new ComboBox<>();
        modeMenuButton.minWidthProperty().bind(toolbarPane.minWidthProperty().subtract(TOOL_BAR_PAD));
        modeMenuButton.setPromptText(PropertyManager.getManager().getPropertyValue(MODE_MENU_TEXT.toString()));
        toolbarPane.getChildren().add(modeMenuButton);

    }

    private void initializeToolbarHandlers(AppTemplate app) throws InstantiationException {
        try {
            Class<?>       klass                        = Class.forName("controller." + app.getControllerClass());
            Constructor<?> constructor                  = klass.getConstructor(AppTemplate.class);
            appController = (AppController) constructor.newInstance(app);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }

        profileCreateButton.setOnAction(e -> appController.handleProfileCreate());
        profileViewButton.setOnAction(e -> appController.handleProfileView());
        loginButton.setOnAction(e -> appController.handleLogin());
        logoutButton.setOnAction(e -> appController.handleLogout());
        modeMenuButton.setOnAction(e -> appController.handleModeMenu());
        helpButton.setOnAction(e -> appController.handleHelp());
        closeButton.setOnMouseClicked(e -> appController.handleClose());
        startButton.setOnAction(e -> appController.handleStart());
        homeButton.setOnAction(e -> appController.handleHome());




        //modeMenuButton.setItems(FXCollections.observableArrayList(new ArrayList<String>(){{add("Dictionary");add("Famous People");add("Popular Food");}}));
        modeMenuButton.setItems(FXCollections.observableList(appController.getModeList()));

    }

    public String getSelection(){
        String selection = modeMenuButton.getSelectionModel().getSelectedItem();
        if(selection==null)
            return "";
        else
            return selection;
    }

    private void initializeGroupings(){
        NOT_LOGGED_HOME_SET.add(profileCreateButton);
        NOT_LOGGED_HOME_SET.add(loginButton);
        NOT_LOGGED_HOME_SET.add(helpButton);
        //NOT_LOGGED_HOME_SET.add(closeButton);

        LOGGED_HOME_SET.add(profileViewButton);
        LOGGED_HOME_SET.add(logoutButton);
        LOGGED_HOME_SET.add(modeMenuButton);
        LOGGED_HOME_SET.add(startButton);
        LOGGED_HOME_SET.add(helpButton);
        //LOGGED_HOME_SET.add(closeButton);

        STARTED_SET.add(profileViewButton);
        STARTED_SET.add(homeButton);
        STARTED_SET.add(helpButton);
        //STARTED_SET.add(closeButton);
    }

    public void updateWorkspaceToolbar(boolean savable) {

    }

    // INITIALIZE THE WINDOW (i.e. STAGE) PUTTING ALL THE CONTROLS
    // THERE EXCEPT THE WORKSPACE, WHICH WILL BE ADDED THE FIRST
    // TIME A NEW Page IS CREATED OR LOADED
    private void initializeWindow() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();

        // GET THE SIZE OF THE SCREEN
        Screen      screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        appPane = new BorderPane();
        appPane.setLeft(toolbarPane);
        appPane.setRight(rightToolbarPane);
        primaryScene = appSpecificWindowWidth < 1 || appSpecificWindowHeight < 1 ? new Scene(appPane)
                                                                                 : new Scene(appPane,
                                                                                             appSpecificWindowWidth,
                                                                                             appSpecificWindowHeight);

        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resrouces folder does not exist.");
        try (InputStream appLogoStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(APP_LOGO)))) {
            primaryStage.getIcons().add(new Image(appLogoStream));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        primaryScene.setOnKeyPressed(e->{
            if(e.isControlDown()){
                if(e.getCode() == KeyCode.Q)
                    appController.handleClose();
                else if(e.getCode() == KeyCode.H)
                    appController.handleHome();
            }
        });

        primaryStage.setScene(primaryScene);
        primaryStage.setWidth(primaryScene.getWidth());
        primaryStage.setHeight(primaryScene.getHeight());
        primaryStage.centerOnScreen();
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setResizable(false);
        primaryStage.show();
    }
    
    /**
     * This is a public helper method for initializing a simple button with
     * an icon and tooltip and placing it into a toolbar.
     *
     * @param toolbarPane Toolbar pane into which to place this button.
     * @param icon        Icon image file name for the button.
     * @param tooltip     Tooltip to appear when the user mouses over the button.
     * @param disabled    true if the button is to start off disabled, false otherwise.
     * @return A constructed, fully initialized button placed into its appropriate
     * pane container.
     */
    public Button initializeChildButton(Pane toolbarPane, String icon, String tooltip, boolean disabled) throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();

        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resources folder does not exist.");

        Button button = new Button();
        try  {
            if(icon != null) {
                InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(icon)));
                Image buttonImage = new Image(imgInputStream);
                button.setGraphic(new ImageView(buttonImage));
            }
            button.setDisable(disabled);
            Tooltip buttonTooltip = new Tooltip(propertyManager.getPropertyValue(tooltip));
            button.setTooltip(buttonTooltip);
            button.minWidthProperty().bind(toolbarPane.minWidthProperty().subtract(TOOL_BAR_PAD));
            toolbarPane.getChildren().add(button);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        return button;
    }

    public Button initializeChildButton(Pane toolbarPane, String icon, String tooltip, String text, boolean disabled) throws IOException{
        Button button = initializeChildButton(toolbarPane, icon, tooltip, disabled);
        button.setText(PropertyManager.getManager().getPropertyValue(text));
        return button;
    }

    public void initializeOtherNodes(){

    }

    public void initializeDialogSingletons(Stage primaryStage){

    }

    public void toggleLogButton(){
        //set tooltip
        //set image
    }

    public void addToWorkspace(Pane pane){
        appPane.setCenter(pane);
    }

    public void clearWorkspaceGUI(){
        appPane.setCenter(null);
    }

    public boolean isWorkspaceEmpty(){
        return appPane.getCenter() == null;
    }

    public void setButtons(Node...buttons){
        setButtons(buttons);
    }

    public void setButtons(List<Node> buttons){
        toolbarPane.getChildren().setAll(buttons);
    }

    /**
     * This function specifies the CSS style classes for the controls managed
     * by this framework.
     */
    @Override
    public void initStyle() {
        // currently, we do not provide any stylization at the framework-level
    }



    public Button getProfileCreateButton() {
        return profileCreateButton;
    }

    public Button getProfileViewButton() {
        return profileViewButton;
    }

    public Button getLoginButton() {
        return loginButton;
    }

    public Button getLogoutButton() {
        return logoutButton;
    }

    public Button getHelpButton() {
        return helpButton;
    }

    public Button getCloseButton() {
        return closeButton;
    }

    public Button getStartButton() {
        return startButton;
    }

    public Button getHomeButton() {
        return homeButton;
    }

    public ComboBox<String> getModeMenuButton() {
        return modeMenuButton;
    }

}
