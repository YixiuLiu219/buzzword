package components;

/**
 * This interface provides the behavior required for a builder
 * object used for initializing all components for this application.
 * This is one means of employing a component hierarchy.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public interface AppComponentsBuilder<D, F> {

    D buildDataComponent() throws Exception;

    F buildFileComponent() throws Exception;

    AppGuiStylizer buildStylizerComponent();
}
