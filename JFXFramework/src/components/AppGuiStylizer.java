package components;

import ui.AppGUI;

/**
 * Created by Yixiu Liu on 11/8/2016.
 */
public interface AppGuiStylizer {

    /**
     * This function will be called at start up to initializes
     * the looks of the appgui.
     *
     * @param appGUI
     */
    void initStyle(AppGUI appGUI);
}
