package data;

/**
 * Created by Yixiu Liu on 11/26/2016.
 */
public enum WorkspaceObjectState {
    IN_GAME, PAUSED, IN_TITLE, WIN, LOST;
}
