package data;

import javafx.scene.layout.Pane;


/**
 * Created by Yixiu Liu on 11/7/2016.
 */
public interface WorkspaceObject {
    void pause();
    void resume();
    void terminate();
    Pane getPane();
    WorkspaceObjectState getState();
}
