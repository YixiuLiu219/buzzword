package data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.deploy.net.URLEncoder;
import com.sun.xml.internal.messaging.saaj.packaging.mime.util
        .BASE64DecoderStream;
import com.sun.xml.internal.messaging.saaj.packaging.mime.util.BASE64EncoderStream;
import components.AppDataComponent;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Path;

/**
 * Created by Yixiu Liu on 11/7/2016.
 */
public class ProfileIO{
    protected final String FOLDER_SEPARATOR = "/";
    protected final String FILE_EXTENSION = ".json";
    protected final String NAME_SEPARATOR = "%%";
    private final String CIPHER_KEY = "16byteciphercode";
    private final String CHARSET = "UTF8";

    public ProfileIO(){

    }

    public boolean saveData(ProfileData data, Path filePath, boolean overWrite) throws IOException {
        /*
        ObjectMapper objectMapper = new ObjectMapper();

        File folder = filePath.toFile();
        String saveName = data.getName() + NAME_SEPARATOR + data.getPassword() + FILE_EXTENSION;
        File saveFile = new File(folder.toString() + FOLDER_SEPARATOR + saveName).getAbsoluteFile();

        for(String name: folder.list()){
            name = name.split(NAME_SEPARATOR)[0];
            if(data.getName().toUpperCase().equals(name.toUpperCase())){ //if existed username
                if(!overWrite) {
                    return false;
                }
            }
        }

        objectMapper.writerWithDefaultPrettyPrinter().writeValue(saveFile, data);
        return true;
        */
        ///*
        try {
            ObjectMapper mapper = new ObjectMapper();
            ProfileData saveInstance = ProfileData.cloneInstance(data);

            //get name
            String name = saveInstance.getName();
            String password = saveInstance.getPassword();

            //encrypt
            String encryptedName = encode(name);
            String encryptedPass = encode(password);

            //set encrypt info
            saveInstance.setName(encryptedName);
            saveInstance.setPassword(encryptedPass);

            File folder = filePath.toFile();
            String saveName = encryptedName + FILE_EXTENSION;
            File newSaveFile = new File(folder.toString() + FOLDER_SEPARATOR + saveName).getAbsoluteFile();

            for(String iterName: folder.list()){
                try {
                    iterName = iterName.split(FILE_EXTENSION)[0];
                    String encryptedIterName = encode(iterName);

                    if (encryptedName.equals(encryptedIterName)) { //if existed username
                        if (!overWrite) {
                            return false;
                        }
                    }
                }catch (UnsupportedEncodingException e){
                    //do nothing
                }
            }

            mapper.writerWithDefaultPrettyPrinter().writeValue(newSaveFile, saveInstance);
            return true;

        }catch (IOException e){
            e.printStackTrace();
            throw new IOException();
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        //*/
    }

    public void loadData(ProfileData data, Path filePath) throws IOException {
        /*
        ObjectMapper objectMapper = new ObjectMapper();

        File folder = filePath.toFile();
        String saveName = data.getName() + NAME_SEPARATOR + data.getPassword() + FILE_EXTENSION;
        File saveFile = new File(folder.toString() + FOLDER_SEPARATOR + saveName).getAbsoluteFile();


        for(String name: folder.list()){
            if(saveName.toUpperCase().equals(name.toUpperCase())){
                data.setData(objectMapper.readValue(saveFile, ProfileData.class));
                return;
            }
        }

        throw new IOException("PROFILE NOT EXISTS ERROR AT LOAD_DATA IN PROFILEIO");
        */
        ObjectMapper objectMapper = new ObjectMapper();
        File folder = filePath.toFile();

        String saveNameToLookFor = encode(data.getName());
        File saveFile = new File(folder.toString() + FOLDER_SEPARATOR + saveNameToLookFor + FILE_EXTENSION).getAbsoluteFile();

        for(String iterName: folder.list()){
            try {
                String encryptedIterName = iterName.split(FILE_EXTENSION)[0];

                if (saveNameToLookFor.equals(encryptedIterName)) { //if existed username
                    ProfileData temp = objectMapper.readValue(saveFile, ProfileData.class);
                    if(data.getPassword().equals(decode(temp.getPassword()))){
                        String displayName = decode(temp.getName());
                        String displayPassword = decode(temp.getPassword());
                        temp.setName(displayName);
                        temp.setPassword(displayPassword);
                        data.setData(temp);
                        return;
                    }
                }
            }catch (UnsupportedEncodingException e){
                e.printStackTrace();
            }
        }

        throw new IOException("PROFILE NOT EXISTS");
    }

    public void exportData(AppDataComponent<ProfileData> data, Path filePath) throws IOException {

    }

    public boolean exists(AppDataComponent<ProfileData> data, Path filePath) throws IOException {
        return false;
    }

    private String encode(String string) throws UnsupportedEncodingException {
        return URLEncoder.encode(new String(BASE64EncoderStream.encode(string.getBytes("UTF-8"))), "UTF-8");
    }

    private String decode(String string) throws UnsupportedEncodingException {
        return new String(BASE64DecoderStream.decode(URLDecoder.decode(string, "UTF-8").getBytes()));
    }


}
