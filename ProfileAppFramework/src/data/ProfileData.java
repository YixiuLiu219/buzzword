package data;

import java.util.*;

/**
 * Created by Yixiu Liu on 11/21/2016.
 */
public class ProfileData{
    private static final String SEPARATOR = "->";

    public static final ProfileData cloneInstance(ProfileData source){
        ProfileData data = new ProfileData();
        data.name = source.name;
        data.password = source.password;
        data.unlockedLevels.putAll(source.unlockedLevels);
        data.bestScores.putAll(source.bestScores);
        return data;
    }

    private String name = "";
    private String password = "";
    private HashMap<String, ArrayList<String>> unlockedLevels = new HashMap<> ();
    private HashMap<String, Integer> bestScores = new HashMap<> ();

    public ProfileData(){

    }

    public void addUnlockedLevel(String mode, String level){
        if(!unlockedLevels.containsKey(mode)){
            unlockedLevels.put(mode, new ArrayList<String>(){{add(level);}});
        }
        else{
            if(!unlockedLevels.get(mode).contains(level))
                unlockedLevels.get(mode).add(level);
        }
    }

    public void addBestScore(String mode, String level, Integer score){
        String key = mode+SEPARATOR+level;
        if(bestScores.containsKey(key)){
            bestScores.remove(key);
        }
        bestScores.put(key, score);
    }

    public String toString(){
        String s = getName()+" "+getPassword()+"\n"+"Completed:\n";
        for(String key: unlockedLevels.keySet()){
            s+="\t"+key+":"+ unlockedLevels.get(key)+"\n";
        }
        return s;
    }

    public void reset() {
        name = "";
        password = "";
        unlockedLevels.clear();
        bestScores.clear();
    }

    public void setData(ProfileData data){
        name = data.name;
        password = data.password;
        unlockedLevels = data.unlockedLevels;
        bestScores = data.bestScores;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public HashMap<String, ArrayList<String>> getUnlockedLevels() {
        return unlockedLevels;
    }

    public void setUnlockedLevels(HashMap<String, ArrayList<String>>
                                          unlockedLevels) {
        this.unlockedLevels = unlockedLevels;
    }

    public HashMap<String, Integer> getBestScores() {
        return bestScores;
    }

    public void setBestScores(HashMap<String, Integer> bestScores) {
        this.bestScores = bestScores;
    }
}
