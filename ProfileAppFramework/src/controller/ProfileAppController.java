package controller;

import apptemplate.AppTemplate;
import data.ProfileData;
import data.ProfileIO;
import data.WorkspaceObject;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import ui.AppGUI;
import view.HelpDialogSingleton;
import view.ProfileDialogSingleton;
import view.ProfileViewSingleton;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created by Yixiu Liu on 11/7/2016.
 */
public class ProfileAppController implements AppController {
    protected AppTemplate<ProfileData, ProfileIO> appTemplate;
    protected ArrayList<WorkspaceObject> workspaceObjectList = new ArrayList<WorkspaceObject>();
    protected final String PROFILE_DIR = "ProfileAppFramework/resources/profiles";
    protected final String PROFILE_FOLDER_MISSING_ERROR = "Profile folder is missing";
    protected final String PROFILE_EXISTS_ERROR = "The user already exists";
    protected final String PROFILE_NOT_EXIST_ERROR = "User does not exist";
    protected final String PROFILE_IO_ERROR = "IO error";
    protected final String PROFILE_ILLEGAL_ERROR = "Illegal characters";
    private final Pattern ILLEGAL_CHARS = Pattern.compile("[^A-Za-z0-9]");
    private boolean loggedIn = false;

    //Handlers

    private EventHandler<ActionEvent> createProfileConfirmFunction = actionEvent -> {
        ProfileDialogSingleton singleton = ProfileDialogSingleton.getSingleton();

        //get data from singleton
        String name = singleton.getName();
        String password = singleton.getPassword();

        //check illegal characters
        if(!isProfileLegal(name) || !isProfileLegal(password) || name.length()<=0 || password.length()<=0){
            singleton.setMessage(PROFILE_ILLEGAL_ERROR);
            singleton.show();
            return;
        }

        //new data
        ProfileData newData = new ProfileData();
        newData.setName(name);
        newData.setPassword(password);
        additionalCreateProfileModifications(newData);

        //try save data
        File saveFolder = new File(PROFILE_DIR).getAbsoluteFile();
        if(!saveFolder.exists()){
            singleton.setMessage(PROFILE_FOLDER_MISSING_ERROR);
            singleton.show();
            return;
        }

        try {
            boolean success = appTemplate.getFileComponent().saveData(newData, saveFolder.toPath(), false);
            if (!success) {
                singleton.setMessage(PROFILE_EXISTS_ERROR);
                singleton.show();
                return;
            }
        }catch (IOException e){
            singleton.setMessage(PROFILE_IO_ERROR);
            singleton.show();
            return;
        }

        //set data
        ProfileData currentData = appTemplate.getDataComponent();
        currentData.setData(newData);

        //hide window
        singleton.hide();

        //show appropriate buttons in main GUI
        AppGUI gui = appTemplate.getGUI();
        gui.getProfileViewButton().setText(newData.getName());
        gui.setButtons(gui.LOGGED_HOME_SET);
        loggedIn = true;

    };

    private EventHandler<ActionEvent> loginProfileConfirmFunction = actionEvent -> {
        ProfileDialogSingleton singleton = ProfileDialogSingleton.getSingleton();

        //get data from singleton
        String name = singleton.getName();
        String password = singleton.getPassword();

        //check illegal characters
        if(!isProfileLegal(name) || !isProfileLegal(password) || name.length()<=0 || password.length()<=0){
            singleton.setMessage(PROFILE_ILLEGAL_ERROR);
            singleton.show();
            return;
        }

        //new data
        ProfileData newData = new ProfileData();
        newData.setName(name);
        newData.setPassword(password);

        //try load data
        File saveFolder = new File(PROFILE_DIR).getAbsoluteFile();
        if(!saveFolder.exists()){
            singleton.setMessage(PROFILE_FOLDER_MISSING_ERROR);
            return;
        }

        try {
            appTemplate.getFileComponent().loadData(newData, saveFolder.toPath());
        }catch (IOException e){
            singleton.setMessage(PROFILE_NOT_EXIST_ERROR);
            return;
        }

        //set data
        ProfileData currentData = appTemplate.getDataComponent();
        currentData.setData(newData);
        additionalLoginModifications(currentData);

        //hide window
        singleton.hide();

        //show appropriate buttons in main GUI
        AppGUI gui = appTemplate.getGUI();
        gui.getProfileViewButton().setText(newData.getName());
        gui.setButtons(gui.LOGGED_HOME_SET);
        loggedIn = true;
    };

    public void saveCurrentData(){
        ProfileDialogSingleton singleton = ProfileDialogSingleton.getSingleton();
        File saveFolder = new File(PROFILE_DIR).getAbsoluteFile();
        if(!saveFolder.exists()){
            singleton.setMessage(PROFILE_FOLDER_MISSING_ERROR);
            return;
        }

        try {
            appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), saveFolder.toPath(), true);
        }catch (IOException e){
            singleton.setMessage(PROFILE_NOT_EXIST_ERROR);
            return;
        }
    }

    public ProfileAppController(AppTemplate<ProfileData, ProfileIO> appTemplate) {
        this.appTemplate = appTemplate;
    }

    public AppGUI getAppGUI(){
        return appTemplate.getGUI();
    }

    public void destroyWorkspace(){
        for(WorkspaceObject o : workspaceObjectList)
            o.terminate();
        workspaceObjectList.clear();
        appTemplate.getGUI().clearWorkspaceGUI();
    }

    public void setWorkspaceObject(WorkspaceObject workspaceObject){
        if(workspaceObjectList.size()<=0)
            workspaceObjectList.add(null);
        workspaceObjectList.set(0, workspaceObject);
        appTemplate.getGUI().addToWorkspace(workspaceObject.getPane());
    }

    @Override
    public void handleNewRequest() {

    }

    @Override
    public void handleSaveRequest() throws IOException {

    }

    @Override
    public void handleLoadRequest() throws IOException {

    }

    @Override
    public void handleExitRequest() {

    }
///////////////////////////////////////////////////////////
    @Override
    public void handleProfileCreate() {
        ProfileDialogSingleton singleton = ProfileDialogSingleton.getSingleton();
        singleton.setTitleText("CREATE PROFILE");
        singleton.show(createProfileConfirmFunction);
    }

    @Override
    public void handleProfileView() {
        for(WorkspaceObject o: workspaceObjectList)
            o.pause();
        ProfileViewSingleton.getSingleton().show(appTemplate.getDataComponent());
        for(WorkspaceObject o: workspaceObjectList)
            o.resume();
    }

    @Override
    public void handleLogin() {
        ProfileDialogSingleton singleton = ProfileDialogSingleton.getSingleton();
        singleton.setTitleText("LOGIN");
        singleton.show(loginProfileConfirmFunction);
    }

    @Override
    public void handleLogout() {
        AppGUI gui = appTemplate.getGUI();
        gui.setButtons(gui.NOT_LOGGED_HOME_SET);
        gui.loadTitleScreen();

        loggedIn = false;
    }

    @Override
    public void handleModeMenu() {

    }

    @Override
    public void handleHelp() {
        for(WorkspaceObject o : workspaceObjectList)
            o.pause();
        HelpDialogSingleton.getSingleton().show();
        for(WorkspaceObject o : workspaceObjectList)
            o.resume();
    }

    @Override
    public void handleClose() {
        destroyWorkspace();
        AppGUI gui = appTemplate.getGUI();
        gui.getWindow().close();
    }

    @Override
    public void handleStart() {
        AppGUI gui = appTemplate.getGUI();
        gui.setButtons(gui.STARTED_SET);
        stageRequiringComponentInit(gui.getWindow());
    }

    @Override
    public void handleHome() {
        if(!loggedIn)
            return;

        AppGUI gui = appTemplate.getGUI();
        gui.setButtons(gui.LOGGED_HOME_SET);
        destroyWorkspace();
        gui.loadTitleScreen();
    }

    @Override
    public ArrayList<String> getModeList(){
        return new ArrayList<>();
    }

    public WorkspaceObject getWorkspaceObject(){
        if(workspaceObjectList.size()<=0)
            return null;
        return workspaceObjectList.get(0);
    }

    protected boolean isProfileLegal(String string){
        return !ILLEGAL_CHARS.matcher(string).find();
    }

    protected void additionalCreateProfileModifications(ProfileData data){

    }

    protected void additionalLoginModifications(ProfileData data){

    }

    protected void stageRequiringComponentInit(Stage mainStage){

    }
}
