package profileapp;

import apptemplate.AppTemplate;
import components.*;
import data.ProfileData;
import data.ProfileIO;
import view.ProfileAppStylizer;

/**
 * Created by Yixiu Liu on 11/7/2016.
 */
public class ProfileApp extends AppTemplate<ProfileData, ProfileIO> {

    public static void main(String[]args){
        launch();
    }

    @Override
    public String getControllerClass(){
        return "ProfileAppController";
    }

    @Override
    public AppComponentsBuilder<ProfileData, ProfileIO> makeAppBuilderHook() {
        return new AppComponentsBuilder<ProfileData, ProfileIO>() {

            @Override
            public ProfileData buildDataComponent() throws Exception {
                return new ProfileData();
            }

            @Override
            public ProfileIO buildFileComponent() throws Exception {
                return new ProfileIO();
            }

            @Override
            public AppGuiStylizer buildStylizerComponent(){
                return new ProfileAppStylizer();
            }
        };
    }
}
