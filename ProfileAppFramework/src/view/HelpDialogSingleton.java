package view;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Created by Yixiu Liu on 11/8/2016.
 */
public class HelpDialogSingleton extends Stage {
    private static HelpDialogSingleton singleton = new HelpDialogSingleton();

    private Scene dialogScene;
    private ScrollPane scrollPane;
    private VBox instructionPane;
    private Text titleText;
    private Text wordContainerLabel;
    private VBox container;

    private Button back;

    private HelpDialogSingleton() {

    }

    public static HelpDialogSingleton getSingleton() {
        if (singleton == null)
            singleton = new HelpDialogSingleton();
        return singleton;
    }

    public void addText(String text){
        instructionPane.getChildren().add(newText(text));
    }

    public void setTitleText(String title){
        titleText.setText(title);
    }

    public void init(Stage primaryStage) {
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        this.centerOnScreen();

        //init nodes
        container = new VBox();
        titleText = new Text("HELP WINDOW");
        wordContainerLabel = new Text();
        scrollPane = new ScrollPane();
        instructionPane = new VBox();
        back = new Button("CLOSE");
        dialogScene = new Scene(container);

        back.setOnAction(e->this.hide());

        //add
        container.getChildren().addAll(titleText, wordContainerLabel, scrollPane, back);
        scrollPane.setContent(instructionPane);

        titleText.setStyle("-fx-font-size:24; " +
                "-fx-font-family: Arial; " +
                "-fx-fill: rgba(255, 255, 255, 0.8);");
        wordContainerLabel.setStyle("-fx-font-size:16; " +
                "-fx-font-family: Arial; " +
                "-fx-fill: rgba(0, 100, 100, 0.5);");

        scrollPane.setPrefViewportWidth(400);
        scrollPane.setPrefViewportHeight(400);
        scrollPane.setStyle("-fx-background:rgba(0,50,50,0.5)");

        instructionPane.minWidthProperty().bind(scrollPane.prefViewportWidthProperty());
        instructionPane.minHeightProperty().bind(scrollPane.prefViewportHeightProperty());
        instructionPane.prefWidthProperty().bind(scrollPane.prefViewportWidthProperty());
        instructionPane.prefHeightProperty().bind(scrollPane.prefViewportHeightProperty());
        instructionPane.setStyle("-fx-alignment: center;");

        back.setStyle("-fx-base:gray;");

        container.setAlignment(Pos.CENTER);
        container.setPrefHeight(500);
        container.setPrefWidth(500);
        container.setStyle("-fx-padding: 10px; -fx-spacing:20px; -fx-background-color:rgba(50,50,50,0.8); ");


        dialogScene.setFill(Color.TRANSPARENT);
        this.initStyle(StageStyle.TRANSPARENT);
        this.setScene(dialogScene);

        addText("WELCOME TO BUZZWORD AND ENJOY YOUR STAY!");
    }

    private Text newText(String text){
        Text newText = new Text(text);
        newText.setStyle("-fx-fill:WHITE");
        return newText;
    }
}
