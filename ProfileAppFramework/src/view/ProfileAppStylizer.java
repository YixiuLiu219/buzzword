package view;

import components.AppGuiStylizer;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import propertymanager.PropertyManager;
import ui.AppGUI;

import static view.ProfileAppProperties.*;

/**
 * Created by Yixiu Liu on 11/8/2016.
 */
public class ProfileAppStylizer implements AppGuiStylizer {

    @Override
    public void initStyle(AppGUI gui) {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));
        gui.getRightToolbarPane().setId(propertyManager.getPropertyValue(RIGHT_TOOLBAR_ID));

        //ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        //toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        //toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));
    }
}
