package view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import ui.YesNoCancelDialogSingleton;

/**
 * Created by Yixiu Liu on 11/8/2016.
 */
public class ProfileDialogSingleton extends Stage{
    private static ProfileDialogSingleton singleton;

    private Scene dialogScene;
    private VBox mainContainer;
    private GridPane container;

    private Button submitButton;
    private Button cancelButton;
    private Button editButton;

    private TextField nameField;
    private TextField passwordField;

    private Text nameText;
    private Text passwordText;

    private Text titleText;
    private Text messageText;

    private ProfileDialogSingleton(){}

    public static ProfileDialogSingleton getSingleton(){
        if(singleton==null)
            singleton = new ProfileDialogSingleton();
        return singleton;
    }

    public void show(EventHandler<ActionEvent> confirmButtonFunction){
        submitButton.setOnAction(confirmButtonFunction);
        showAndWait();
    }

    public String getName(){
        return nameField.getText().trim();
    }

    public String getPassword(){
        return passwordField.getText().trim();
    }

    public void setMessage(String message){
        messageText.setText(message);
    }

    public void setTitleText(String title){ titleText.setText(title); }

    public void init(Stage primaryStage) {
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        this.centerOnScreen();

        //init nodes

        titleText = new Text();
        submitButton = new Button();
        cancelButton = new Button();
        editButton = new Button();
        nameField = new TextField();
        passwordField = new TextField();
        nameText = new Text();
        passwordText = new Text();
        messageText = new Text();
        container = new GridPane();
        mainContainer = new VBox();

        container.add(titleText, 0,0);
        container.add(nameField,1,1);
        container.add(passwordField,1,2);
        container.add(nameText,0,1);
        container.add(passwordText,0,2);
        container.add(submitButton,0,3);
        container.add(cancelButton,1,3);
        //container.add(messageText, 0, 4);
        mainContainer.getChildren().addAll(container, messageText);
        dialogScene = new Scene(mainContainer);

        //attach functionalities
        cancelButton.setOnAction(e->{
            nameField.clear();
            passwordField.clear();
            messageText.setText("");
            this.hide();
        });

        this.setOnHiding(e->{
            nameField.clear();
            passwordField.clear();
            messageText.setText("");
            this.hide();
        });

        submitButton.setDefaultButton(true);

        //styles
        submitButton.setText("SUBMIT");
        cancelButton.setText("CANCEL");
        nameText.setText("NAME:");
        passwordText.setText("PASSWORD:");

        nameText.setStyle("-fx-fill:WHITE");
        passwordText.setStyle("-fx-fill:WHITE");
        titleText.setStyle("-fx-fill:WHITE");
        messageText.setStyle("-fx-fill:RED");

        submitButton.setStyle("-fx-base: gray;");
        cancelButton.setStyle("-fx-base: gray;");

        //submitButton.setOnMouseEntered();

        container.setVgap(10);
        container.setHgap(10);
        mainContainer.setStyle("-fx-padding: 50; spacing: 10px; -fx-alignment: center; -fx-background-color:rgba(0,0,0,0.7); ");
        dialogScene.setFill(Color.TRANSPARENT);

        this.initStyle(StageStyle.TRANSPARENT);
        this.setScene(dialogScene);
    }

}
