package view;

/**
 * Created by Yixiu Liu on 11/8/2016.
 */
public enum ProfileAppProperties {
    ROOT_BORDERPANE_ID,
    TOP_TOOLBAR_ID,
    SEGMENTED_BUTTON_BAR,
    RIGHT_TOOLBAR_ID,
    FIRST_TOOLBAR_BUTTON,
    LAST_TOOLBAR_BUTTON,
    WORKSPACE_HEADING_LABEL,
    HEADING_LABEL;
}
