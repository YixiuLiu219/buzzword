package buzzword;

import profileapp.ProfileApp;

import static javafx.application.Application.launch;

/**
 * Created by Yixiu Liu on 11/8/2016.
 */
public class BuzzwordApp extends ProfileApp {

    public static void main(String[]args){
        launch();
    }

    @Override
    public String getControllerClass(){
        return "BuzzwordAppController";
    }
}
