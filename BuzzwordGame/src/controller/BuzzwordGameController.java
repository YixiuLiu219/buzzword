package controller;

import behavior.Observable;
import behavior.Observer;
import data.*;
import javafx.application.Platform;
import javafx.scene.layout.Pane;
import view.*;

import java.util.*;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;

/**
 * Created by Yixiu Liu on 11/8/2016.
 */
public class BuzzwordGameController implements WorkspaceObject, Observable<BuzzwordGameController>, Observer<StopWatch> {

    //Final field stuff
    //private final String MODE_FOLDER = "BuzzwordGame/resources/modelist";
    //private final String FILE_SEPARATOR = "/";
    //private static final int MIN_LETTERS = 3;
    //private static final int MAX_LETTERS = 16;
    private static final int MIN_LEVEL = 1;
    private static final int MAX_LEVEL = 8;
    private static final int BASE_TARGET_SCORE = 600;
    private static final int SCORE_INCREMENT_PER_LEVEL = 100;
    private static final int BOARD_SIZE = 4;
    private static final int TIME = 900;
    private static final int TIMER_DECREMENT = 1;
    private static final Pattern ILLEGAL_CHARACTERS = Pattern.compile("[^A-Za-z]");

    //
    private String modeName;
    private WorkspaceObjectState state;
    private LinkedList<Observer<BuzzwordGameController>> observerList = new LinkedList<>();

    //GUI stuff
    private BuzzwordGameGUI gameGUI;
    private BuzzwordGameplayScreen gameplayScreen;
    private BuzzwordLevelScreen levelScreen;
    private TimerGUI timerGUI;

    //Control input stuff
    private ArrayList<Pair> path = new ArrayList<>();
    private ArrayList<String> inputLetterList = new ArrayList<>();

    //Profile data stuff
    private ArrayList<String> unlockedLevelNames = new ArrayList<>();
    private boolean typingInProgress = false;

    //Level data stuff
    private Set<String> wordList = new HashSet<>();
    private BuzzwordLevel[] levels;
    private BuzzwordLevel currentLevel;

    //util
    private StopWatch stopWatch;


    public BuzzwordGameController(String mode, ArrayList<String> initUnlockedLevelNames, Set<String> modeWords){
        unlockedLevelNames.addAll(initUnlockedLevelNames);
        wordList = modeWords;
        modeName = mode;
        state = WorkspaceObjectState.IN_TITLE;

        //set gui
        gameGUI = new BuzzwordGameGUI(mode);
        levelScreen = gameGUI.getLevelSelect();
        gameplayScreen = gameGUI.getGameplay();

        //set level screen unlcoked levels
        for(int i=MIN_LEVEL; i<=MAX_LEVEL; i++){
            String levelName = i+"";
            levelScreen.makeLevelButton(levelName, !unlockedLevelNames.contains(levelName));
        }

        //init handling functions
        initHandler();

        //prepare file (wordlist)
        /*
        try {
            File file = new File(MODE_FOLDER+FILE_SEPARATOR+mode).getAbsoluteFile();
            Scanner sc = new Scanner(file);
            sc.useDelimiter("\\Z");
            for(String s: sc.next().split("\\n")){
                s = s.trim();
                if(s.length()>=MIN_LETTERS && s.length()<=MAX_LETTERS) {
                    wordList.add(s.toUpperCase());
                }
            }
            sc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        */

        //prepare data
        /*
        levels = new BuzzwordLevel[MAX_LEVEL-MIN_LEVEL+1];
        for(int i=MIN_LEVEL-1; i<MAX_LEVEL; i++){
            int levelNum = i+1;
            BuzzwordLevel level = new BuzzwordLevel(levelNum+"", BOARD_SIZE, BASE_TARGET_SCORE+levelNum*SCORE_INCREMENT_PER_LEVEL, wordList);
            levels[i] = level;
        }
        */

        //utils
        timerGUI = gameplayScreen.getTimerGUI();
        stopWatch = new StopWatch(TIME, TIMER_DECREMENT);
        stopWatch.addObserver(this);

        //do this last
        gameGUI.getChildren().setAll(levelScreen);
    }

    private void initHandler(){
        //level button handler
        for(BuzzwordNode node: levelScreen.getLevelList()){
            node.setOnMouseClicked(e->levelButtonFunction(node));
        }

        //play pause handler
        gameplayScreen.setOnPlayPauseAction(e->{
            if(state == WorkspaceObjectState.IN_GAME)
                pause();
            else
                resume();
        });

        //restart handler
        gameplayScreen.setOnRestartPressed(()->{
            playLevel(getInfoPackage().currentLevelName);
        });

        //type field submit handler
        gameplayScreen.setOnTypingFieldType(
                ()->{
                    typingInProgress = false;
                    String word = gameplayScreen.getTypingField().getText().trim().toUpperCase();
                    inputWord(word);
                    gameplayScreen.getTypingField().clear();
                },
                (observableValue,oldValue,newValue)->{
                    typingInProgress = newValue.length()>0;
                    BuzzwordNode[][] buzzwordNodes = gameplayScreen.getBuzzwordNodeMatrix();
                    gameplayScreen.clearBuzzwordMatrixHighlight();
                    highlightNodes(newValue.trim().toUpperCase(), buzzwordNodes);
                }
        );

        gameplayScreen.setOnDrag(e->{
            if(typingInProgress) { //optimization if-statement
                gameplayScreen.getTypingField().clear();
                BuzzwordNode[][] buzzwordNodes = gameplayScreen.getBuzzwordNodeMatrix();
                for(int i=0; i<buzzwordNodes.length; i++) {
                    for (int j = 0; j < buzzwordNodes[i].length; j++) {
                        buzzwordNodes[i][j].dehighlight();
                    }
                }
                typingInProgress = false;
            }

            gameplayScreen.getTypingField().setDisable(true);

            int row = (int)(e.getY()/ gameplayScreen.getNodeSize());
            int col = (int)(e.getX()/ gameplayScreen.getNodeSize());
            BuzzwordNode[][] buzzwordNodes = gameplayScreen.getBuzzwordNodeMatrix();

            if(row>=0 && row<buzzwordNodes.length && col>=0 && col<buzzwordNodes.length ){

                Pair currentPoint = new Pair(row, col);
                BuzzwordNode currentNode = buzzwordNodes[row][col];

                if(currentNode.getNodeType() == BuzzwordNodeType.LETTER){
                    if(path.size()<=0){
                        path.add(currentPoint);
                        buzzwordNodes[row][col].circleHighlight();
                    }
                    else{
                        Pair diff = path.get(path.size()-1).dst(currentPoint);
                        Pair prev = path.get(path.size()-1);
                        if(diff.row>=0 && diff.row<=2 && diff.col>=0 && diff.col<=2 && !currentNode.isHighlighted()){
                            buzzwordNodes[row][col].circleHighlight();
                            if(prev.slope(currentPoint)>=0){
                                Pair mid = prev.mid(currentPoint);
                                buzzwordNodes[mid.row][mid.col].edgeHighlight();
                            }
                            else{
                                Pair mid = prev.mid(currentPoint);
                                buzzwordNodes[mid.row][mid.col].edge2Highlight();
                            }
                            path.add(currentPoint);
                        }
                    }
                }
            }
        });

        gameplayScreen.setOnDragStop(()->{
            BuzzwordNode[][] buzzwordNodes = gameplayScreen.getBuzzwordNodeMatrix();
            String word = "";
            for(Pair pair: path){
                word += buzzwordNodes[pair.row][pair.col].getLetter();
            }
            inputWord(word);
            resetDrag();
            gameplayScreen.getTypingField().setDisable(false);
        });
    }

    private void highlightNodes(String word, BuzzwordNode[][] buzzwordNodes) {
        ArrayList<Pair> path = new ArrayList<>();
        boolean[][] markMap = new boolean[buzzwordNodes.length][buzzwordNodes.length];
        for(int i=0; i<markMap.length; i++)
            for(int j=0; j<markMap.length; j++)
                markMap[i][j] = false;

        for(int i=0; i<buzzwordNodes.length; i+=2) {
            for (int j = 0; j < buzzwordNodes.length; j += 2) {
                if(word.length()>0)
                    if(buzzwordNodes[i][j].getLetter().equals(word.charAt(0)+""))
                        highlightNodesHelper(i, j, word, buzzwordNodes, path, markMap);
            }
        }
    }

    private void highlightNodesHelper(int row, int col, String word, BuzzwordNode[][] buzzwordNodes, ArrayList<Pair> path, boolean[][] markMap){
        path.add(new Pair(row, col));

        if(word.length() <= 1){
            //if(buzzwordNodes[row][col].getLetter().equals(word)) {
                for (int i = 0; i < path.size(); i++) {
                    Pair iter = path.get(i);
                    //System.out.print(buzzwordNodes[iter.row][iter.col].getLetter());
                    //System.out.print("-");
                    if (i <= 0) {
                        buzzwordNodes[iter.row][iter.col].circleHighlight();
                    }
                    else {
                        Pair diff = path.get(i - 1).dst(iter);
                        Pair prev = path.get(i - 1);
                        if (diff.row >= 0 && diff.row <= 2 && diff.col >= 0 && diff.col <= 2) {

                            buzzwordNodes[iter.row][iter.col].circleHighlight();
                            if (prev.slope(iter) >= 0) {
                                Pair mid = prev.mid(iter);
                                buzzwordNodes[mid.row][mid.col].edgeHighlight();
                            }
                            else {
                                Pair mid = prev.mid(iter);
                                buzzwordNodes[mid.row][mid.col].edge2Highlight();
                            }
                        }
                    }
                }
                //System.out.println();
            //}
            path.remove(path.size()-1);

            return;
        }

        markMap[row][col] = true;
        word = word.substring(1);

        for(int i=row-2; i<=row+2; i+=2){
            for(int j=col-2; j<=col+2; j+=2){
                if(i>=0 && j>=0 && i<buzzwordNodes.length && j<buzzwordNodes.length){
                    if(!markMap[i][j] && buzzwordNodes[i][j].getLetter().equals(word.charAt(0)+"")){
                        highlightNodesHelper(i, j, word, buzzwordNodes, path, markMap);
                    }
                }
            }
        }

        markMap[row][col] = false;
        path.remove(path.size()-1);
    }

    private void resetDrag(){
        BuzzwordNode[][] buzzwordNodes = gameplayScreen.getBuzzwordNodeMatrix();
        for(int i=0; i<buzzwordNodes.length; i++) {
            for (int j = 0; j < buzzwordNodes[i].length; j++) {
                buzzwordNodes[i][j].dehighlight();
            }
        }
        path.clear();
    }

    private void inputWord(String word){
        boolean goodWord = currentLevel.resolveWord(word);

        if(goodWord){
            gameplayScreen.addToScore(word, currentLevel.getPointsWorth(word));
        }
        else{

        }
        gameplayScreen.getTypingField().clear();

        if(currentLevel.getCurrentScore()>=currentLevel.getTargetScore()){
            state = WorkspaceObjectState.WIN;
            stopWatch.terminate();
            notifyObservers();
        }
    }

    private void levelButtonFunction(BuzzwordNode node){
        playLevel(node.getLetter());
    }

    public void setUnlockedLevels(Collection<String> levels){
        unlockedLevelNames.clear();
        unlockedLevelNames.addAll(levels);

        for(int i=0; i<unlockedLevelNames.size(); i++){
            String name = unlockedLevelNames.get(i);
            levelScreen.setLevelDisable(name, false);
        }
    }

    /*
    public void highlightAll(String word){
        BuzzwordNode[][] buzzwordNodes = gameplayScreen.getBuzzwordNodeMatrix();

        if(row>=0 && row<buzzwordNodes.length && col>=0 && col<buzzwordNodes.length ){

            Pair currentPoint = new Pair(row, col);
            BuzzwordNode currentNode = buzzwordNodes[row][col];

            if(currentNode.getNodeType() == BuzzwordNodeType.LETTER){
                if(path.size()<=0){
                    path.add(currentPoint);
                    buzzwordNodes[row][col].circleHighlight();
                }
                else{
                    Pair diff = path.get(path.size()-1).dst(currentPoint);
                    Pair prev = path.get(path.size()-1);
                    if(diff.row>=0 && diff.row<=2 && diff.col>=0 && diff.col<=2 && !currentNode.isHighlighted()){
                        buzzwordNodes[row][col].circleHighlight();
                        if(prev.slope(currentPoint)>=0){
                            Pair mid = prev.mid(currentPoint);
                            buzzwordNodes[mid.row][mid.col].edgeHighlight();
                        }
                        else{
                            Pair mid = prev.mid(currentPoint);
                            buzzwordNodes[mid.row][mid.col].edge2Highlight();
                        }
                        path.add(currentPoint);
                    }
                }
            }
        }
    }
    */

    public void showLevelSelect(){
        gameGUI.getChildren().setAll(levelScreen);
    }

    public String getMode(){
        return modeName;
    }

    public void playLevel(String levelName) {
        System.out.println(Thread.activeCount());
        state = WorkspaceObjectState.IN_GAME;

        int levelNum = Integer.parseInt(levelName)-1;
        currentLevel = new BuzzwordLevel(levelName, BOARD_SIZE, BASE_TARGET_SCORE+levelNum*SCORE_INCREMENT_PER_LEVEL, wordList);

        stopWatch.reset();
        resetDrag();
        gameplayScreen.setModeName(modeName);
        gameplayScreen.setTargetScore(currentLevel.getTargetScore());
        gameplayScreen.setCurrentScore(currentLevel.getCurrentScore());
        gameplayScreen.clearRecord();
        gameplayScreen.getTimerGUI().setTime(stopWatch.getCurrentTime());
        gameplayScreen.setLevelName(currentLevel.getLevelName());
        gameplayScreen.getTypingField().setDisable(false);
        String[][] letterMap = currentLevel.getLetterMap();
        for(int i=0; i<letterMap.length; i++){
            for(int j=0; j<letterMap[i].length; j++){
                gameplayScreen.setLetters(i,j,letterMap[i][j]);
            }
        }

        gameGUI.getChildren().setAll(gameplayScreen);

        stopWatch.start();
    }

    private boolean isLegal(String word){
        return !ILLEGAL_CHARACTERS.matcher(word).find();
    }

    @Override
    public void pause() {
        stopWatch.pause();
        state = WorkspaceObjectState.PAUSED;
        gameGUI.getGameplay().setBoardVisible(false);
        gameGUI.getGameplay().setLevelControls("PLAY");
    }

    @Override
    public void resume() {
        stopWatch.start();
        state = WorkspaceObjectState.IN_GAME;
        gameGUI.getGameplay().setBoardVisible(true);
        gameGUI.getGameplay().setLevelControls("PAUSE");
    }

    @Override
    public void terminate() {
        stopWatch.terminate();
        path.clear();
    }

    @Override
    public Pane getPane() {
        return gameGUI;
    }

    @Override
    public WorkspaceObjectState getState() {
        return state;
    }

    @Override
    public void addObserver(Observer<BuzzwordGameController> observer) {
        observerList.add(observer);
    }

    @Override
    public void notifyObservers() {
        for(Observer<BuzzwordGameController> o: observerList){
            o.beNotified(this);
        }
    }

    @Override
    public void removeObserver(Observer<BuzzwordGameController> observer) {
        observerList.remove(observer);
    }

    public BuzzwordGameInfoPackage getInfoPackage() {
        String nextLevelName = null;
        for(int i=0; i<levelScreen.getLevelList().size(); i++){
            if(levelScreen.getLevelList().get(i).getLetter().equals(currentLevel.getLevelName())){
                if(i < levelScreen.getLevelList().size()-1){
                    nextLevelName = levelScreen.getLevelList().get(i+1).getLetter();
                    break;
                }
            }
        }
        return new BuzzwordGameInfoPackage(nextLevelName, currentLevel.getLevelName(), currentLevel.getCurrentScore(), currentLevel.getPossibleWords());
    }

    @Override
    public void beNotified(StopWatch observable) {
        int currentTime = observable.getCurrentTime();
        Platform.runLater(()->{
            timerGUI.setTime(currentTime);
            if(currentTime <= 0){
                stopWatch.terminate();
                state = WorkspaceObjectState.LOST;
                notifyObservers();
            }
        });
    }
}
