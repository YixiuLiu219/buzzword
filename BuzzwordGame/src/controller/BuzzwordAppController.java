package controller;

import apptemplate.AppTemplate;
import behavior.Observer;
import data.BuzzwordGameInfoPackage;
import data.ProfileData;
import data.WorkspaceObjectState;
import javafx.stage.Stage;
import ui.AppGUI;
import ui.YesNoCancelDialogSingleton;
import view.BuzzwordNode;
import view.GameEndSingleton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by Yixiu Liu on 11/8/2016.
 */
public class BuzzwordAppController extends ProfileAppController implements Observer<BuzzwordGameController> {
    private final String FILE_SEPARATOR = "/";
    private static final int MIN_LETTERS = 3;
    private static final int MAX_LETTERS = 16;
    private static final Pattern LEGAL_WORD = Pattern.compile("[^A-Za-z]");

    private ArrayList<String> modeList = new ArrayList<>();
    private final String MODE_FOLDER = "BuzzwordGame/resources/modelist";
    private final String INITIAL_UNLOCKED_LEVEL = "1";
    private final String INVALID_MODE_ERROR = "Please select an valid mode";
    private final String WIN_MESSAGE = "CONGRADULATIONS! YOU WIN!";
    private final String LOSE_MESSAGE = "YOU LOSE";
    private HashMap<String, Set<String>> modeWordMap = new HashMap<>();


    public BuzzwordAppController(AppTemplate appTemplate){
        super(appTemplate);

        File modeFolder = new File(MODE_FOLDER).getAbsoluteFile();
        if(modeFolder.exists()){
            if(modeFolder.isDirectory()) {
                for (File subFile : modeFolder.listFiles()) {
                    if(subFile.isFile()) {
                        String modeName = subFile.getName().toUpperCase();
                        modeList.add(subFile.getName().toUpperCase());

                        try {
                            Set<String> newWordList = new HashSet<>();
                            File file = new File(MODE_FOLDER+FILE_SEPARATOR+modeName).getAbsoluteFile();
                            Scanner sc = new Scanner(file);
                            sc.useDelimiter("\\Z");
                            for(String s: sc.next().split("\\n")){
                                s = s.trim();
                                if(s.length()>=MIN_LETTERS && s.length()<=MAX_LETTERS && isLegal(s)) {
                                    newWordList.add(s.toUpperCase());
                                }
                            }
                            sc.close();
                            modeWordMap.put(modeName, newWordList);
                            System.out.println(modeName);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    private boolean isLegal(String string){
        return !LEGAL_WORD.matcher(string).find();
    }

    @Override
    protected void additionalCreateProfileModifications(ProfileData data){
        for(String mode: modeList)
            data.addUnlockedLevel(mode, INITIAL_UNLOCKED_LEVEL);
    }

    @Override
    protected void additionalLoginModifications(ProfileData data){
        for(String mode: modeList) {
            if(!data.getUnlockedLevels().containsKey(mode))
                data.addUnlockedLevel(mode, INITIAL_UNLOCKED_LEVEL);
        }
    }

    @Override
    public void handleStart(){

        String selectedMode = super.getAppGUI().getSelection();
        selectedMode = "".equals(selectedMode)? modeList.size()<=0? null: modeList.get(0) : selectedMode;

        if(selectedMode != null) {
            ProfileData data = super.appTemplate.getDataComponent();
            ArrayList<String> unlockedLevelNames = data.getUnlockedLevels().get(selectedMode);

            BuzzwordGameController gameController = new BuzzwordGameController(selectedMode, unlockedLevelNames, modeWordMap.get(selectedMode));
            gameController.addObserver(this);
            super.setWorkspaceObject(gameController);
            super.handleStart();
        }
        else{
            YesNoCancelDialogSingleton.getSingleton().show("", INVALID_MODE_ERROR);
        }
    }

    @Override
    public void handleClose(){
        if(super.getWorkspaceObject() != null) {
            if(super.getWorkspaceObject().getState() != WorkspaceObjectState.IN_TITLE) {
                super.getWorkspaceObject().pause();

                YesNoCancelDialogSingleton singleton = YesNoCancelDialogSingleton.getSingleton();

                singleton.show("", "Are you sure you want to exit?");

                if (singleton.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
                    AppGUI gui = super.appTemplate.getGUI();
                    gui.getWindow().close();
                }
                else{
                    super.getWorkspaceObject().resume();
                }
            }
            else {
                AppGUI gui = super.appTemplate.getGUI();
                gui.getWindow().close();
            }
        }
        else {
            AppGUI gui = super.appTemplate.getGUI();
            gui.getWindow().close();
        }
    }

    @Override
    public ArrayList<String> getModeList(){
        return modeList;
    }

    @Override
    public void beNotified(BuzzwordGameController observable) {
        BuzzwordGameInfoPackage info = observable.getInfoPackage();
        ProfileData data = appTemplate.getDataComponent();

        switch(observable.getState()){
            case WIN:
                if(info.nextLevelName != null)
                    data.addUnlockedLevel(observable.getMode(), info.nextLevelName);
                super.saveCurrentData();

                observable.setUnlockedLevels(data.getUnlockedLevels().get(observable.getMode()));

                GameEndSingleton.getSingleton().show(WIN_MESSAGE, info.wordList,
                        ()-> {
                            observable.showLevelSelect();
                        },
                        ()->{
                            observable.playLevel(info.nextLevelName);
                        }
                );
                break;

            case LOST:
                GameEndSingleton.getSingleton().show(LOSE_MESSAGE, info.wordList,
                        ()-> {
                            observable.showLevelSelect();
                        }
                );
                break;
            default: break;
        }
    }

    @Override
    protected void stageRequiringComponentInit(Stage mainStage){
        GameEndSingleton endDialog = GameEndSingleton.getSingleton();
        if(!endDialog.isInitilialized()){
            endDialog.init(mainStage);
        }
    }
}
