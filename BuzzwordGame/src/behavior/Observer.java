package behavior;

/**
 * Created by Yixiu Liu on 12/6/2016.
 */
public interface Observer<T> {
     void beNotified(T observable);
}
