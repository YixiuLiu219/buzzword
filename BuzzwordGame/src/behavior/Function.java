package behavior;

/**
 * Created by Yixiu Liu on 12/4/2016.
 */
public interface Function {
    void execute();
}
