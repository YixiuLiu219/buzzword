package behavior;

import java.util.LinkedList;

/**
 * Created by Yixiu Liu on 12/6/2016.
 */
public interface Observable<T> {

     void addObserver(Observer<T> observer);

     void notifyObservers();

    void removeObserver(Observer<T> observer);

}
