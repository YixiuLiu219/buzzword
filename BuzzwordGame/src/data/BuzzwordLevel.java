package data;

import java.util.*;

/**
 * Created by Yixiu Liu on 11/26/2016.
 */
public class BuzzwordLevel {
    private static int SCORE_PER_LETTER = 100;
    private static ArrayList<String> letterFrequencyArray = new ArrayList<>(100000);
    private String levelName;
    private int targetScore;
    private int currentScore;
    private String[][] letterMap;
    private Set<String> completedWords = new HashSet<>();
    private Set<String> minimumExpectedWords = new HashSet<>();
    private Set<String> possibleWords = new HashSet<>();
    private int size;

    public BuzzwordLevel(String name, int size, int score, Set<String> allowedWords){
        if(letterFrequencyArray.size()<=0)
            populateFrequencyArray();

        levelName = name;
        this.size = size;
        letterMap = new String[size][size];
        targetScore = score;
        do {
            generateBoard(score, allowedWords);
        }while(targetScore>totalScorePossible(possibleWords));

        debugPrint();
    }

    public boolean resolveWord(String word){
        if(!completedWords.contains(word) && possibleWords.contains(word)){
            currentScore += getPointsWorth(word);
            completedWords.add(word);
            return true;
        }
        return false;
    }

    public int getCurrentScore(){
        return currentScore;
    }

    public Set<String> getPossibleWords(){
        return possibleWords;
    }

    private void generateBoard(int minimumScore, Set<String> allowedWords){

        //set enough words
        int currentScore = 0;
        String[] arr = new String[allowedWords.size()];
        arr = allowedWords.toArray(arr);
        do{
            int randomNum = (int)(Math.random()*arr.length);
            String word = arr[randomNum];
            if(!minimumExpectedWords.contains(word)) {
                currentScore += getPointsWorth(word);
                minimumExpectedWords.add(word);
            }
        }while(currentScore<minimumScore);


        boolean[][] markMap = new boolean[letterMap.length][letterMap.length];
        for(int i=0; i<markMap.length; i++){
            for(int j=0; j<markMap[i].length; j++){
                markMap[i][j] = false;
            }
        }

        ArrayList<Pair> path = new ArrayList<>();
        for(Iterator<String> iter = minimumExpectedWords.iterator(); iter.hasNext();){
            //reset map
            for(int i=0; i<markMap.length; i++){
                for(int j=0; j<markMap[i].length; j++){
                    markMap[i][j] = false;
                }
            }

            //clear path
            path.clear();

            //temp random
            int rand = (int)(Math.random()*2);

            //find empty
            String word = iter.next();
            loop:
            for(int i=rand; i<letterMap.length; i++){
                for(int j=0; j<letterMap[i].length; j++){
                    if(letterMap[i][j] == null){
                        if(putWord(i,j, word, letterMap, markMap, path)){
                            for(int z=0; z<path.size(); z++){
                                Pair pair = path.get(z);
                                letterMap[pair.row][pair.col] = word.charAt(z)+"";
                            }
                            break loop;
                        }
                    }
                }
            }
        }

        //fill empty spaces
        for(int i=0; i<letterMap.length; i++){
            for(int j=0; j<letterMap[i].length; j++){
                if(letterMap[i][j] == null){
                    int num = (int) (Math.random() * letterFrequencyArray.size());
                    letterMap[i][j] = letterFrequencyArray.get(num);
                }
            }
        }

        //Set<String> test = new HashSet<>();
        //test.add(letterMap[0][0]+letterMap[0][1]+letterMap[0][2]+letterMap[0][3]+letterMap[1][2]);
        possibleWords = findWords(letterMap, allowedWords);
    }

    private int totalScorePossible(Collection<String> list){
        int score = 0;
        for(String s: list){
            score += getPointsWorth(s);
        }
        return score;
    }


    private boolean putWord(int row, int col, String currentString, String[][] letterMap, boolean[][] markMap, ArrayList<Pair> path) {
        if(currentString.length()<=0)
            return true;

        markMap[row][col] = true;
        Pair pos = new Pair(row, col);
        path.add(pos);

        String headLetter = currentString.substring(0,1);
        currentString = currentString.substring(1);

        loop:
        for(int i=row-1; i<=row+1; i++){
            for(int j=col-1; j<=col+1; j++){

                if(i>=0 && j>=0 && i<letterMap.length && j<letterMap.length){
                    if(!markMap[i][j]){
                        if(letterMap[i][j] == null || letterMap[i][j].equals(headLetter)) {
                            boolean done = putWord(i, j, currentString, letterMap, markMap, path);
                            if(done)
                                return true;

                        }
                    }
                }

            }
        }
        /*
        for(int i=0; i<path.size(); i++){
            System.out.println(path.get(i));
        }
        System.out.println("+++++++++++++++++++++++++");
        */
        markMap[row][col] = false;
        path.remove(pos);
        return false;
    }

    private boolean findWord(int row, int col, String word, String[][] letterMap, boolean[][] markMap) {
        if(word.length() <= 0){
            return true;
        }

        markMap[row][col] = true;
        String head = word.charAt(0)+"";
        word = word.substring(1);

        for(int i=row-1; i<=row+1; i++){
            for(int j=col-1; j<=col+1; j++){
                if(i>=0 && j>=0 && i<letterMap.length && j<letterMap.length){
                    if(!markMap[i][j] && letterMap[i][j].equals(head)){
                        if(findWord(i, j, word, letterMap, markMap)){
                            markMap[row][col] = false;
                            return true;
                        }
                    }
                }
            }
        }

        markMap[row][col] = false;
        return false;
    }

    private Set<String> findWords(String[][] letterMap, Set<String> allowedWords){
        Set<String> foundWords = new HashSet<>();
        boolean[][] pathMap = new boolean[letterMap.length][letterMap.length];
        for(int i=0; i<pathMap.length; i++){
            for(int j=0; j<pathMap[i].length; j++){
                pathMap[i][j] = false;
            }
        }

        for(Iterator<String> iter = allowedWords.iterator(); iter.hasNext();){
            String word = iter.next();
            loop:
            for(int i=0; i<pathMap.length; i++){
                for(int j=0; j<pathMap[i].length; j++){
                    if(letterMap[i][j].charAt(0) == word.charAt(0)){
                        if(findWord(i, j, word.substring(1), letterMap, pathMap)){
                            foundWords.add(word);
                            break loop;
                        }
                    }
                }
            }
        }
        return foundWords;
    }

    public int getPointsWorth(String word){
        return word.length()*100;
    }

    public int getTargetScore() {
        return targetScore;
    }

    public String[][] getLetterMap() {
        return letterMap;
    }

    public Set<String> getCompletedWords() {
        return completedWords;
    }

    public String getLevelName() {
        return levelName;
    }


    private static void populateFrequencyArray(){
        populate("A", 8167);
        populate("B", 1492);
        populate("C", 2782);
        populate("D", 4253);
        populate("E", 12702);
        populate("F", 2228);
        populate("G", 2015);
        populate("H", 6094);
        populate("I", 6966);
        populate("J", 153);
        populate("K", 772);
        populate("L", 4025);
        populate("M", 2406);
        populate("N", 6749);
        populate("O", 7507);
        populate("P", 1929);
        populate("Q", 95);
        populate("R", 5987);
        populate("S", 6327);
        populate("T", 9056);
        populate("U", 2758);
        populate("V", 978);
        populate("W", 2360);
        populate("X", 15);
        populate("Y", 1974);
        populate("Z", 74);
    }

    private static void populate(String letter, int times){
        for(int i=0; i<times; i++){
            letterFrequencyArray.add(letter);
        }
    }




    private void debugPrint(){
        for(Iterator<String> i = possibleWords.iterator(); i.hasNext();){
            System.out.println(i.next());
        }
        System.out.println("===========");
        for(String i[]: letterMap){
            for(String j: i){
                System.out.print(j);
            }
            System.out.println();
        }
        System.out.println("===========");
    }
}
