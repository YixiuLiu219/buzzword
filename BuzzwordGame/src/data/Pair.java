package data;

/**
 * Created by Yixiu Liu on 12/7/2016.
 */
public class Pair{
    public final int row;
    public final int col;
    public Pair(int row, int col){
        this.row = row;
        this.col = col;
    }

    public Pair dst(Pair pair){
        int r = row - pair.row;
        int c = col - pair.col;
        if(r<0) r *= -1;
        if(c<0) c *= -1;
        return new Pair(r, c);
    }

    public double slope(Pair end){
        int r = end.row - row;
        int c = end.col - col;
        if(c==0)
            return 0;
        return r/c;
    }

    public Pair mid(Pair end){
        int r = (end.row - row)/2+row;
        int c = (end.col - col)/2+col;
        return new Pair(r, c);
    }

    public Pair div(int num){
        int r = row/num;
        int c = col/num;
        return new Pair(r, c);
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof Pair){
            Pair x = (Pair)o;
            return row==x.row && col ==x.col;
        }
        return false;
    }

    @Override
    public String toString(){
        return "("+row+","+col+")";
    }
}

