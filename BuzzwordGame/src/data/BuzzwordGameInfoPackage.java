package data;

import java.util.Collection;

/**
 * Created by Yixiu Liu on 12/7/2016.
 */
public class BuzzwordGameInfoPackage {
        public final Collection<String> wordList;
        public final String nextLevelName;
        public final String currentLevelName;
        public final int score;

        public BuzzwordGameInfoPackage(String nextLevelName, String currentLevelName, int score, Collection<String> wordList){
            this.nextLevelName = nextLevelName;
            this.currentLevelName = currentLevelName;
            this.score = score;
            this.wordList = wordList;
        }

}
