package data;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Created by Yixiu Liu on 11/10/2016.
 */
public class Word {
    private SimpleStringProperty word = new SimpleStringProperty();
    private SimpleIntegerProperty score = new SimpleIntegerProperty();

    public Word(String word, int score){
        this.word.set(word);
        this.score.set(score);
    }

    public String getWord(){
        return word.get();
    }

    public int getScore(){
        return score.get();
    }
}
