package data;

import behavior.Observable;
import behavior.Observer;

import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Yixiu Liu on 12/6/2016.
 */
public class StopWatch implements Observable<StopWatch> {
    private LinkedList<Observer<StopWatch>> observerList = new LinkedList<>();

    private ReentrantLock lock = new ReentrantLock();

    private int totalTime = 0;
    private int currentTime = 0;
    private int decrement = 0;
    private Timer timer;
    private TimerTask task;

    public StopWatch(int totalTime, int decrementPerSecond){
        this.totalTime = totalTime;
        this.decrement = decrementPerSecond;
        currentTime = totalTime;
        timer = new Timer();
        task = new TimerTask(){
            @Override
            public void run() {
                setCurrentTime(currentTime-decrement);
                for(Observer<StopWatch> o: observerList){
                    o.beNotified(StopWatch.this);
                }
            }
        };
    }

    public void start(){
        task.cancel();
        timer.cancel();
        timer = new Timer();
        task = new TimerTask(){
            @Override
            public void run() {
                setCurrentTime(currentTime-decrement);
                for(Observer<StopWatch> o: observerList){
                    o.beNotified(StopWatch.this);
                }
            }
        };
        timer.scheduleAtFixedRate(task,1000, 1000);
    }

    public void terminate(){
        task.cancel();
        timer.cancel();
        timer.purge();
        reset();
    }

    public void reset(){
        setCurrentTime(totalTime);
    }

    public void pause(){
        task.cancel();
        timer.cancel();
    }

    public int getCurrentTime(){
        return currentTime;
    }

    private void setCurrentTime(int time){
        lock.lock();
        currentTime = time;
        lock.unlock();
    }

    public static void main(String[]args){
        StopWatch sw = new StopWatch(2, 1);
        sw.addObserver(new Observer<StopWatch>() {
            int count = 2;
            @Override
            public void beNotified(StopWatch bindingObject) {
                int time = sw.getCurrentTime();
                System.out.println(time);
                if(time <=0 ){
                    count--;
                    sw.terminate();
                    if(count>0)
                        sw.start();
                }
            }
        });
        sw.start();
        sw.reset();
    }

    public void clearObservers(){
        observerList.clear();
    }

    @Override
    public void addObserver(Observer<StopWatch> observer) {
        observerList.add(observer);
    }

    @Override
    public void notifyObservers() {
        for(Observer<StopWatch> o : observerList)
            o.beNotified(this);
    }

    @Override
    public void removeObserver(Observer<StopWatch> observer) {
        observerList.remove(observer);
    }
}
