package view;

import data.Word;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

/**
 * Created by Yixiu Liu on 11/8/2016.
 */
public class BuzzwordGameGUI extends Pane {
    BuzzwordLevelScreen levelSelect;
    BuzzwordGameplayScreen gameplay;

    public BuzzwordGameGUI(String mode){
        levelSelect = new BuzzwordLevelScreen(mode);
        gameplay = new BuzzwordGameplayScreen();
    }

    public BuzzwordLevelScreen getLevelSelect() {
        return levelSelect;
    }

    public BuzzwordGameplayScreen getGameplay() {
        return gameplay;
    }














    public Pane makeLevelScreen(String mode){
        VBox vb = new VBox();
        VBox levelBox = new VBox();
        Text titleText = new Text("BUZZWORD");
        Text modeText = new Text(mode);


        DropShadow ds = new DropShadow();
        Color highLight = Color.BLUE;
        double highLightRadius = 50;
        ds.setColor(highLight);
        ds.setRadius(highLightRadius);
        ds.setBlurType(BlurType.GAUSSIAN);

        FlowPane fp = new FlowPane();
        fp.setPrefWrapLength(250);
        int counter =0;
        for(int i=0; i<2; i++){
            for(int j=0; j<4; j++){

                StackPane sp = new StackPane();
                Text text = new Text(counter+"");
                text.setFill(Color.WHITE);

                double nodeRadius = 30;
                Circle circle = new Circle(nodeRadius);
                circle.setStyle("-fx-fill: rgba(0, 100, 100, 1);");
                circle.setOnMouseClicked(e->{
                    levelText.setText("level "+text.getText());
                    this.getChildren().setAll(gameplay);
                });

                circle.setOnMouseEntered(e->circle.setEffect(ds));
                circle.setOnMouseExited(e->circle.setEffect(null));

                sp.getChildren().addAll(text, circle);
                text.toFront();
                text.setDisable(true);


                //disableNode
                if(!mode.equals("") && i>=mode.charAt(0)-'C'){
                    circle.setDisable(true);
                    circle.setStyle("-fx-fill: rgba(100, 100, 100, 0.5);");
                }


                fp.getChildren().add(sp);
                counter++;
            }
        }

        titleText.setStyle(
                "-fx-font-size:60; " +
                "-fx-font-family: Arial; " +
                "-fx-fill: rgba(0, 100, 100, 0.5);" +
                        "-fx-fill: linear-gradient(to right, rgba(0, 100, 100, 0.2), rgba(0, 100, 100, 1));"
        );

        modeText.setStyle(
                "-fx-font-size:30; " +
                "-fx-font-family: Arial; " +
                "-fx-fill: rgba(0, 100, 100, 0.5);"
        );

        fp.setHgap(50);
        fp.setVgap(50);
        fp.setPrefWrapLength((60+fp.getHgap())*4);

        levelBox.getChildren().addAll(modeText, fp);
        levelBox.setStyle("-fx-spacing: 20px;");
        levelBox.setAlignment(Pos.CENTER);

        vb.getChildren().addAll(titleText, levelBox);
        vb.setAlignment(Pos.CENTER);
        vb.setPadding(new Insets(0,0,0,250));
        vb.setStyle("-fx-spacing: 150px;");

        return vb;
    }


    Text levelText = new Text("What level text");
    public Pane makeGameScreen(String mode){
        HBox hb = new HBox();

        VBox center = centerSide(mode);
        VBox rightSide = rightSide();
        VBox topSide = topSide();
        VBox bottomSide = bottomSide();
        VBox leftSide = leftSide();

        hb.getChildren().add(leftSide);
        VBox middleContainer = new VBox();
        middleContainer.getChildren().add(topSide);
        middleContainer.getChildren().add(center);
        middleContainer.getChildren().add(bottomSide);
        VBox rightContainer = new VBox();
        rightContainer.getChildren().add(rightSide);
        hb.getChildren().add(middleContainer);
        hb.getChildren().add(rightContainer);


        //STYLE
        hb.setStyle("-fx-spacing:100;");
        middleContainer.setStyle("-fx-spacing:100");
        topSide.setAlignment(Pos.CENTER);
        center.setAlignment(Pos.CENTER);
        bottomSide.setAlignment(Pos.CENTER);
        middleContainer.setAlignment(Pos.TOP_CENTER);
        return hb;
    }

    private VBox leftSide(){
        VBox vb = new VBox();
        Pane space = new Pane();
        space.setMinSize(120, 2);
        vb.getChildren().add(space);
        return vb;
    }

    private VBox rightSide(){
        VBox vb = new VBox();
        TextField tf = new TextField();
        vb.setStyle("-fx-spacing:20; -fx-alignment:center;");
        vb.getChildren().addAll(timer(), tf, record(), scoreboard());
        return vb;
    }

    private VBox timer(){
        Text title = new Text("TIME LEFT:");
        Text time = new Text("100");
        VBox vb = new VBox();
        vb.getChildren().addAll(title, time);

        //style
        vb.setStyle("-fx-alignment:center;-fx-background-color:rgba(100,100,100,0.5); -fx-background-radius:45;-fx-padding:10");
        title.setStyle("-fx-font-size: 20; -fx-fill:WHITE");
        time.setStyle("-fx-font-size: 20; -fx-fill:WHITE");

        return vb;
    }

    private Pane record(){
        VBox vb = new VBox();

        //SCORE SO FAR
        Text title = new Text();
        Text score = new Text();
        Pane scorePane = new VBox();
        scorePane.getChildren().addAll(title, score);

        //SCORE TABLE
        TableView<Word> tableView = new TableView<>();
        TableColumn<Word, String> wordCol = new TableColumn<>();
        TableColumn<Word, Double> scoreCol = new TableColumn<>();
        ObservableList<Word> wordList = FXCollections.observableArrayList(
                new Word("Test1", 100),
                new Word("Test2", 200)
        );
                wordCol.setCellValueFactory(
                new PropertyValueFactory<Word, String>("word")
        );
        scoreCol.setCellValueFactory(
                new PropertyValueFactory<Word, Double>("score")
        );
        tableView.setItems(wordList);
        tableView.getColumns().addAll(wordCol, scoreCol);
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        //FINALIZE
        vb.getChildren().addAll(tableView, scorePane);

        //Style
        title.setText("Current Total:");
        score.setText("0");
        wordCol.setText("WORDS");
        scoreCol.setText("SCORES");
        wordCol.setStyle("-fx-background-color:rgba(0, 0, 0,0.5)");
        scoreCol.setStyle("-fx-background-color:rgba(0, 0, 0,0.5)");

        scorePane.setStyle("-fx-alignment:center;-fx-background-color:rgba(100,100,100,0.5);-fx-padding:5");
        title.setStyle("-fx-font-size: 15; -fx-fill:WHITE");
        score.setStyle("-fx-font-size: 15; -fx-fill:WHITE");

        return vb;
    }

    private VBox scoreboard(){
        Text title = new Text("TARGET");
        Text score = new Text("0");
        VBox vb = new VBox();
        vb.getChildren().addAll(title, score);

        //Style
        vb.setStyle("-fx-alignment:center;-fx-background-color:rgba(100,100,100,0.5); -fx-padding:10");
        title.setStyle("-fx-font-size: 15; -fx-fill:WHITE");
        score.setStyle("-fx-font-size: 15; -fx-fill:WHITE");

        return vb;
    }



    private VBox bottomSide(){
        VBox vb = new VBox();
        levelText = new Text("DEFAULT");
        Button playPause = new Button("PLAY");
        vb.getChildren().addAll(levelText, playPause);

        playPause.setStyle("-fx-base:rgba(100,100,100, 0.5);");
        levelText.setStyle("-fx-font-size: 30; -fx-fill:WHITE");
        return vb;
    }

    private VBox topSide(){
        VBox vb = new VBox();
        Text title = new Text("BUZZWORD");
        vb.getChildren().addAll(title);

        title.setStyle(
                "-fx-font-size:60; " +
                        "-fx-font-family: Arial; " +
                        "-fx-fill: rgba(0, 100, 100, 0.5);" +
                        "-fx-fill: linear-gradient(to right, rgba(0, 100, 100, 0.2), rgba(0, 100, 100, 1));"
        );
        return vb;
    }

    private VBox centerSide(String mode){
        VBox vb = new VBox();
        Text modeName = new Text(mode);
        Pane grid = buildGrid();
        vb.getChildren().addAll(modeName, grid);

        modeName.setStyle(
                "-fx-font-size:20; " +
                        "-fx-font-family: Arial; " +
                        "-fx-underline: true;"+
                        "-fx-fill: rgba(20, 20, 20, 0.5);"
        );
        return vb;
    }

    private Pane buildGrid(){
        double nodeOffset = 90;
        double nodeRadius = 30;
        String nodeStyle = "-fx-fill: rgba(0, 100, 100, 1);";
        String edgeStyle = "-fx-stroke: rgba(0, 100, 100, 1);";
        String textStyle = "-fx-fill: WHITE; -fx-font-size: 25;";
        double edgeWidth = 2;
        double containerInset = nodeRadius+30;


        DropShadow ds = new DropShadow();
        Color highLight = Color.CYAN;
        double highLightRadius = 50;
        ds.setColor(highLight);
        ds.setRadius(highLightRadius);
        ds.setBlurType(BlurType.GAUSSIAN);

        Circle[][] circleMap = new Circle[4][4];


        Pane container = new Pane();
        //container.setPadding(new Insets(containerInset));

        for(int i=0; i<circleMap.length; i++){
            for(int j=0; j<circleMap[i].length; j++){

                Circle circle = new Circle(nodeRadius);
                circle.setStyle(nodeStyle);
                circle.setLayoutX(i*nodeOffset+containerInset);
                circle.setLayoutY(j*nodeOffset+containerInset);
                container.getChildren().add(circle);
                circleMap[i][j] = circle;

            }
        }

        for(int i=0; i<circleMap.length-1; i++){
            for(int j=0; j<circleMap[i].length-1; j++){

                Line r = new Line();
                r.setStrokeWidth(edgeWidth);
                r.startXProperty().bind(circleMap[i][j].layoutXProperty());
                r.startYProperty().bind(circleMap[i][j].layoutYProperty());
                r.endXProperty().bind(circleMap[i][j+1].layoutXProperty());
                r.endYProperty().bind(circleMap[i][j+1].layoutYProperty());
                container.getChildren().add(r);
                r.setStyle(edgeStyle);
                //
                r = new Line();
                r.setStrokeWidth(edgeWidth);
                r.startXProperty().bind(circleMap[i][j].layoutXProperty());
                r.startYProperty().bind(circleMap[i][j].layoutYProperty());
                r.endXProperty().bind(circleMap[i+1][j+1].layoutXProperty());
                r.endYProperty().bind(circleMap[i+1][j+1].layoutYProperty());
                container.getChildren().add(r);
                r.setStyle(edgeStyle);
                //
                r = new Line();
                r.setStrokeWidth(edgeWidth);
                r.startXProperty().bind(circleMap[i][j].layoutXProperty());
                r.startYProperty().bind(circleMap[i][j].layoutYProperty());
                r.endXProperty().bind(circleMap[i+1][j].layoutXProperty());
                r.endYProperty().bind(circleMap[i+1][j].layoutYProperty());
                container.getChildren().add(r);
                r.setStyle(edgeStyle);
            }
        }
        for(int i=0; i<circleMap.length-1; i++){
            for(int j=1; j<circleMap[i].length; j++){

                Line r = new Line();
                r.setStrokeWidth(edgeWidth);
                r.startXProperty().bind(circleMap[i][j].layoutXProperty());
                r.startYProperty().bind(circleMap[i][j].layoutYProperty());
                r.endXProperty().bind(circleMap[i+1][j-1].layoutXProperty());
                r.endYProperty().bind(circleMap[i+1][j-1].layoutYProperty());
                container.getChildren().add(r);
                r.setStyle(edgeStyle);
            }
        }
        for(int i=1; i<circleMap[3].length; i++){
            Line r = new Line();
            r.setStrokeWidth(edgeWidth);
            r.startXProperty().bind(circleMap[3][i].layoutXProperty());
            r.startYProperty().bind(circleMap[3][i].layoutYProperty());
            r.endXProperty().bind(circleMap[3][i-1].layoutXProperty());
            r.endYProperty().bind(circleMap[3][i-1].layoutYProperty());
            container.getChildren().add(r);
            r.setStyle(edgeStyle);

            r = new Line();
            r.setStrokeWidth(edgeWidth);
            r.startXProperty().bind(circleMap[i][3].layoutXProperty());
            r.startYProperty().bind(circleMap[i][3].layoutYProperty());
            r.endXProperty().bind(circleMap[i-1][3].layoutXProperty());
            r.endYProperty().bind(circleMap[i-1][3].layoutYProperty());
            container.getChildren().add(r);
            r.setStyle(edgeStyle);
        }

        for(int i=0; i<container.getChildren().size(); i++){
            Node n = container.getChildren().get(i);
            n.setOnMouseEntered(e->n.setEffect(ds));
            n.setOnMouseExited(e->n.setEffect(null));
            //n.setOnMouseEntered(e->n.setStyle("-fx-effect: dropshadow(gaussian, red, 50, 0, 0, 0)"));
            //n.setOnMouseExited(e->n.setStyle(""));
        }
        for(int i=0; i<circleMap.length; i++){
            for(int j=0; j<circleMap[i].length; j++){
                circleMap[i][j].toFront();
                Text text = new Text(""+Character.toString((char)(i+j+'A')));
                text.xProperty().bind(circleMap[i][j].layoutXProperty().add(-text.getFont().getSize()/2));
                text.yProperty().bind(circleMap[i][j].layoutYProperty().add(text.getFont().getSize()/2));
                text.setStyle(textStyle);
                text.setDisable(true);
                container.getChildren().add(text);
            }
        }

        return container;
    }

}
