package view;

/**
 * Created by Yixiu Liu on 11/26/2016.
 */
public enum BuzzwordNodeType{
    LETTER, H_EDGE, V_EDGE, EMPTY;
}
