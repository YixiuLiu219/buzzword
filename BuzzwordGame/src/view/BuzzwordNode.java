package view;

import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 * Created by Yixiu Liu on 11/26/2016.
 */
public class BuzzwordNode extends StackPane {
    private static final InnerShadow INNER_SHADOW = new InnerShadow();
    private static final DropShadow DROP_SHADOW = new DropShadow();

    private Rectangle pad = new Rectangle();
    private Text letterText = new Text();
    private Circle circle = new Circle();
    private Rectangle edge = new Rectangle();
    private Rectangle edge2 = new Rectangle();
    private BuzzwordNodeType nodeType;
    private int size = 0;
    private int row;
    private int col;
    private double edgeStrokeSize = 5;
    private boolean highlighted = false;


    public BuzzwordNode(int size, int row, int col, BuzzwordNodeType nodeType){
        super();
        this.size = size;
        this.row = row;
        this.col = col;
        this.nodeType = nodeType;

        pad.setWidth(size);
        pad.setHeight(size);
        circle.setRadius(size/2);
        edge.setWidth(size);
        edge.setHeight(edgeStrokeSize);
        edge2.setWidth(size);
        edge2.setHeight(edgeStrokeSize);

        this.getChildren().add(pad);
        letterText.setDisable(true);

        if(nodeType == BuzzwordNodeType.LETTER) {
            this.getChildren().add(circle);
            this.getChildren().add(letterText);
        }
        else if(nodeType == BuzzwordNodeType.H_EDGE) {
            this.getChildren().add(edge);
        }
        else if(nodeType == BuzzwordNodeType.V_EDGE) {
            edge.setRotate(90);
            this.getChildren().add(edge);
        }
        else if(nodeType == BuzzwordNodeType.EMPTY) {
            edge.setRotate(45);
            edge2.setRotate(-45);
            this.getChildren().add(edge);
            this.getChildren().add(edge2);
        }

        initStyle();
    }

    public BuzzwordNodeType getNodeType(){
        return nodeType;
    }

    public void setLetterText(String letter){
        letterText.setText(letter);
    }

    public String getLetter(){
        return letterText.getText();
    }

    public double getSize(){
        return size;
    }

    private void initStyle(){
        pad.setStyle("-fx-fill: rgba(0, 0, 0, 0);");
        circle.setStyle("-fx-fill: rgba(0, 100, 100, 1);");
        edge.setStyle("-fx-fill: rgba(0, 100, 100, 1);");
        edge2.setStyle("-fx-fill: rgba(0, 100, 100, 1);");
        letterText.setStyle("-fx-fill: WHITE; -fx-font-size: 25;");


        Color highLight = Color.CYAN;
        double highLightRadius = 50;
        DROP_SHADOW.setColor(highLight);
        DROP_SHADOW.setRadius(highLightRadius);
        DROP_SHADOW.setBlurType(BlurType.GAUSSIAN);
        circle.setOnMouseEntered(e->{
            if(!highlighted)
                circle.setEffect(DROP_SHADOW);
        });
        circle.setOnMouseExited(e->{
            if(!highlighted)
                circle.setEffect(null);
        });
    }

    public void resetStyle(){
        initStyle();
    }

    public void disableNode(boolean disable){
        if(disable){
            circle.setStyle("-fx-fill: rgba(100, 100, 100, 0.5);");
        }
        else{
            circle.setStyle("-fx-fill: rgba(0, 100, 100, 1);");
        }
        this.setDisable(disable);
    }

    public void circleHighlight(){
        highlighted = true;
        INNER_SHADOW.setBlurType(BlurType.THREE_PASS_BOX);
        INNER_SHADOW.setColor(Color.WHITE);
        INNER_SHADOW.setRadius(50);
        circle.setEffect(INNER_SHADOW);
    }

    public void edgeHighlight(){
        INNER_SHADOW.setBlurType(BlurType.THREE_PASS_BOX);
        INNER_SHADOW.setColor(Color.WHITE);
        INNER_SHADOW.setRadius(50);
        edge.setEffect(INNER_SHADOW);
    }

    public void edge2Highlight(){
        INNER_SHADOW.setBlurType(BlurType.THREE_PASS_BOX);
        INNER_SHADOW.setColor(Color.WHITE);
        INNER_SHADOW.setRadius(50);
        edge2.setEffect(INNER_SHADOW);
    }

    public void dehighlight(){
        highlighted = false;
        circle.setEffect(null);
        edge.setEffect(null);
        edge2.setEffect(null);
        resetStyle();
    }

    public boolean isHighlighted(){
        return highlighted;
    }
}
