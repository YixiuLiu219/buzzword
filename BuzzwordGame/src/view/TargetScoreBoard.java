package view;

import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Created by Yixiu Liu on 12/6/2016.
 */
public class TargetScoreBoard{
    private final String WORD_COL_TITLE = "WORDS";
    private final String SCORE_COL_TITLE = "SCORES";

    Text title = new Text("TARGET");
    Text score = new Text("0");
    VBox container = new VBox();

    public TargetScoreBoard(){
        init();
        initStyle();
    }

    private void init() {
        container.getChildren().addAll(title, score);
    }

    private void initStyle() {
        container.setStyle("-fx-alignment:center;-fx-background-color:rgba(100,100,100,0.5); -fx-padding:10");
        title.setStyle("-fx-font-size: 15; -fx-fill:WHITE");
        score.setStyle("-fx-font-size: 15; -fx-fill:WHITE");
    }

    public VBox getPane(){
        return container;
    }

    public Text getTitle() {
        return title;
    }

    public Text getScore() {
        return score;
    }
}


