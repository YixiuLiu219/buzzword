package view;

import behavior.Observer;
import data.StopWatch;
import javafx.application.Platform;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Created by Yixiu Liu on 12/6/2016.
 */
public class TimerGUI{
    private final String TIMER_TITLE = "TIME LEFT:";
    private int timerValue = 0;

    private VBox container = new VBox();
    private Text title = new Text();
    private Text time = new Text();

    public TimerGUI() {
        init();
        initStyle();
    }

    private void init(){
        container.getChildren().add(title);
        container.getChildren().add(time);
    }

    private void initStyle(){
        title.setText(TIMER_TITLE);
        time.setText(timerValue+"");

        container.setStyle("-fx-alignment:center;-fx-background-color:rgba(100,100,100,0.5); -fx-background-radius:45;-fx-padding:10");
        title.setStyle("-fx-font-size: 20; -fx-fill:WHITE");
        time.setStyle("-fx-font-size: 20; -fx-fill:WHITE");
    }

    public VBox getPane(){
        return container;
    }

    public Text getTitle() {
        return title;
    }

    public Text getTime() {
        return time;
    }

    public void setTime(int time){
        this.time.setText(time+"");
    }
}
