package view;

import data.Word;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Created by Yixiu Liu on 12/6/2016.
 */
public class ScoreRecord{
    private final String SCORE_TITLE_TEXT = "Current Total:";
    private int totalScore = 0;
    private final String WORD_COL_TITLE = "WORDS";
    private final String SCORE_COL_TITLE = "SCORES";

    private VBox container = new VBox();

    //TOTAL
    Text scoreTitle = new Text();
    Text score = new Text();
    Pane scorePane = new VBox();

    //TABLE
    ObservableList<Word> wordList = FXCollections.observableArrayList();
    TableView<Word> tableView = new TableView<>();
    TableColumn<Word, String> wordCol = new TableColumn<>();
    TableColumn<Word, Integer> scoreCol = new TableColumn<>();

    public ScoreRecord() {
        init();
        initStyle();
    }

    private void init(){
        //score
        score.setText(totalScore+"");
        scorePane.getChildren().addAll(scoreTitle, score);

        //table
        wordCol.setCellValueFactory(
                new PropertyValueFactory<>("word")
        );
        scoreCol.setCellValueFactory(
                new PropertyValueFactory<>("score")
        );
        tableView.setItems(wordList);
        tableView.getColumns().addAll(wordCol, scoreCol);
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);


        //FINALIZE
        container.getChildren().addAll(tableView, scorePane);
    }

    private void initStyle(){
        //Style
        scoreTitle.setText(SCORE_TITLE_TEXT);
        wordCol.setText(WORD_COL_TITLE);
        scoreCol.setText(SCORE_COL_TITLE);

        wordCol.setStyle("-fx-background-color:rgba(0, 0, 0,0.5)");
        scoreCol.setStyle("-fx-background-color:rgba(0, 0, 0,0.5)");

        scorePane.setStyle("-fx-alignment:center;-fx-background-color:rgba(100,100,100,0.5);-fx-padding:5");
        scoreTitle.setStyle("-fx-font-size: 15; -fx-fill:WHITE");
        score.setStyle("-fx-font-size: 15; -fx-fill:WHITE");
    }

    public void addScore(String word, int score){
        wordList.add(new Word(word, score));
        totalScore += score;
        this.score.setText(totalScore+"");
    }

    public void setCurrentScore(int score){
        totalScore = score;
        this.score.setText(totalScore+"");
    }

    public void clearScore(){
        wordList.clear();
    }

    public VBox getPane(){
        return container;
    }
}
