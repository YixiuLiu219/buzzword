package view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.*;
import javafx.scene.text.Text;

import java.util.ArrayList;

/**
 * Created by Yixiu Liu on 11/8/2016.
 */
public class BuzzwordLevelScreen extends VBox {
    private TilePane levelNodeContainer = new TilePane();
    private Text titleText = new Text("BUZZWORD");
    private Text modeText = new Text();
    private ArrayList<BuzzwordNode> levelList = new ArrayList<>();

    public BuzzwordLevelScreen(String modeName){
        super();
        modeText.setText(modeName);
        initPosition();
        initStyle();
    }

    private void initPosition(){
        this.getChildren().addAll(titleText, modeText, levelNodeContainer);
    }

    private void initStyle(){
        this.setAlignment(Pos.TOP_CENTER);
        this.setPadding(new Insets(0,0,0,250));
        this.setStyle("-fx-spacing: 150px;");

        levelNodeContainer.setPrefColumns(4);
        levelNodeContainer.setVgap(30);
        levelNodeContainer.setHgap(30);

        titleText.setStyle(
                "-fx-font-size:60; " +
                        "-fx-font-family: Arial; " +
                        "-fx-fill: rgba(0, 100, 100, 0.5);" +
                        "-fx-fill: linear-gradient(to right, rgba(0, 100, 100, 0.2), rgba(0, 100, 100, 1));"
        );

        modeText.setStyle(
                "-fx-font-size:30; " +
                        "-fx-font-family: Arial; " +
                        "-fx-fill: rgba(0, 100, 100, 0.5);"
        );
    }

    public void makeLevelButton(String levelname, boolean disable){

        BuzzwordNode node = new BuzzwordNode(60, 0, 0, BuzzwordNodeType.LETTER);
        node.setLetterText(levelname);

        //disableNode
        //if(disableNode){
            //node.setCircleStyle("-fx-fill: rgba(100, 100, 100, 0.5);");
            node.disableNode(disable);
        //}

        levelList.add(node);
        levelNodeContainer.getChildren().add(node);
    }

    public void setLevelDisable(String levelname, boolean disable){
        for(BuzzwordNode node: levelList){
            if(node.getLetter().equals(levelname)){
                node.disableNode(disable);
            }
        }
    }


    public ArrayList<BuzzwordNode> getLevelList(){
        return levelList;
    }

}
