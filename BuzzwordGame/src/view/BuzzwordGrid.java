package view;

import javafx.scene.canvas.Canvas;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;

import java.util.Random;

/**
 * Created by Yixiu Liu on 12/6/2016.
 */
public class BuzzwordGrid{
    private TilePane tilePane = new TilePane();
    private Canvas canvas = new Canvas();
    private StackPane container = new StackPane();
    private int tileSize;
    private int actualTileSize;
    private int nodeSize;
    private BuzzwordNode[][] buzzwordNodes;
    private BuzzwordNode[][] buzzwordLetterNodes;

    public BuzzwordGrid(int tileSize, int nodeSize){
        this.tileSize = tileSize;
        this.nodeSize = nodeSize;
        actualTileSize = tileSize*2-1;
        buzzwordNodes = new BuzzwordNode[actualTileSize][actualTileSize];
        buzzwordLetterNodes = new BuzzwordNode[tileSize][tileSize];
        tilePane.setPrefColumns(actualTileSize);
        tilePane.setPrefRows(actualTileSize);

        container.getChildren().addAll(tilePane, canvas);
        canvas.setMouseTransparent(true);
        canvas.setHeight(300);
        canvas.setWidth(300);

        makeTile();
        generateRandomLetters();
    }

    private void makeTile(){
        for(int i=0; i<actualTileSize; i++){
            for(int j=0; j<actualTileSize; j++){
                boolean rowEven = i%2==0;
                boolean colEven = j%2==0;

                BuzzwordNodeType type = BuzzwordNodeType.EMPTY;
                if(rowEven && colEven) type = BuzzwordNodeType.LETTER;
                else if(!rowEven && colEven) type = BuzzwordNodeType.V_EDGE;
                else if(rowEven && !colEven) type = BuzzwordNodeType.H_EDGE;


                BuzzwordNode buzzwordNode = new BuzzwordNode(nodeSize, i, j, type);
                buzzwordNodes[i][j] = buzzwordNode;
                if(buzzwordNode.getNodeType()==BuzzwordNodeType.LETTER)
                    buzzwordLetterNodes[i/2][j/2] = buzzwordNode;
                tilePane.getChildren().add(buzzwordNode);
            }
        }
    }

    public void generateRandomLetters(){
        for(BuzzwordNode[] i: buzzwordNodes){
            for(BuzzwordNode j: i){
                if(j.getNodeType() == BuzzwordNodeType.LETTER){
                    char randomChar = (char)((int)(new Random().nextDouble()*26)+'A');
                    j.setLetterText(randomChar+"");
                }
            }
        }
    }

    public int getTileSize(){
        return actualTileSize;
    }

    public int getNodeSize(){
        return nodeSize;
    }

    public BuzzwordNode[][] getBuzzwordNodes(){
        return buzzwordNodes;
    }

    public BuzzwordNode[][] getBuzzwordLetterNodes(){
        return buzzwordLetterNodes;
    }

    public Pane getPane(){
        return container;
    }

    private void initStyle(){
    }


}

