package view;

import behavior.Function;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Text;


/**
 * Created by Yixiu Liu on 11/8/2016.
 */
public class BuzzwordGameplayScreen extends HBox {
    private final int GRID_SIZE = 4;
    private final int GRID_NODE_SIZE = 60;

    //private Pane grid;
    private TextField typingField = new TextField();
    private Button nextLevelButton;
    private Button replayLevelButton;

    private Text titleText;///WAS HARD CODED In topSIde()
    private Text modeText = new Text();

    private LeftSpacing leftSpacing = new LeftSpacing();
    private TimerGUI timer = new TimerGUI();
    private ScoreRecord scoreRecord = new ScoreRecord();
    private TargetScoreBoard targetScoreBoard = new TargetScoreBoard();
    private LevelControls levelControls = new LevelControls();
    private BuzzwordGrid buzzwordGrid = new BuzzwordGrid(GRID_SIZE, GRID_NODE_SIZE);

    private VBox centerPane = new VBox();
    private VBox topPane = new VBox();
    private VBox rightPane = new VBox();
    private VBox leftPane = new VBox();
    private VBox bottomPane = new VBox();

    private VBox groupOne = new VBox();
    private VBox groupTwo = new VBox();
    private VBox groupThree = new VBox();


    public BuzzwordGameplayScreen(){
        initPosition();
        initStyle();
    }

    public void setTargetScore(int score){
        targetScoreBoard.getScore().setText(score+"");
    }

    public void setLevelName(String levelName){
        levelControls.getLevelText().setText(levelName);
    }

    public void setModeName(String modeName){
        modeText.setText(modeName);
    }

    public void setLetters(int i, int j, String letter){
        buzzwordGrid.getBuzzwordLetterNodes()[i][j].setLetterText(letter);
    }

    public void setOnPlayPauseAction(EventHandler<ActionEvent> e){
        levelControls.getPlayPause().setOnAction(e);
    }

    public void setOnRestartPressed(Function function){
        levelControls.getRestart().setOnAction(e->function.execute());
    }

    /*
    public void setOnTypingFieldSubmit(Function function){
        typingField.setOnKeyPressed(e->{
            if(e.getCode() == KeyCode.ENTER) {
                function.execute();
            }
        });
    }
    */

    public void setOnTypingFieldType(Function submit, ChangeListener<String> textChangeListener){
        typingField.setOnKeyPressed(e->{
            if(e.getCode() == KeyCode.ENTER)
                submit.execute();
        });
        typingField.textProperty().addListener(textChangeListener);
    }

    public void setOnDrag(EventHandler<MouseEvent> handler){
        buzzwordGrid.getPane().setOnMouseDragged(e->{handler.handle(e);});
    }

    public void setOnDragStop(Function function){
        buzzwordGrid.getPane().setOnMouseReleased(e->function.execute());
    }

    public BuzzwordNode[][] getBuzzwordNodeMatrix(){
        return buzzwordGrid.getBuzzwordNodes();
    }

    public void clearBuzzwordMatrixHighlight(){
        BuzzwordNode[][] matrix = buzzwordGrid.getBuzzwordNodes();
        for(int i=0; i<matrix.length; i++){
            for(int j=0; j<matrix[i].length; j++){
                matrix[i][j].dehighlight();
            }
        }
    }

    public int getNodeSize(){
        return buzzwordGrid.getNodeSize();
    }

    public TextField getTypingField() {
        return typingField;
    }

    public void addToScore(String word, int score){
        scoreRecord.addScore(word, score);
    }

    public void clearRecord(){
        scoreRecord.clearScore();
    }

    public void setCurrentScore(int score){
        scoreRecord.setCurrentScore(score);
    }

    public void setBoardVisible(boolean visible){
        buzzwordGrid.getPane().setVisible(visible);
    }

    public void setLevelControls(String text){
        levelControls.getPlayPause().setText(text);
    }

    public TimerGUI getTimerGUI(){
        return timer;
    }




    private void initPosition(){
        leftPane.getChildren().add(leftSpacing.getPane());

        topPane.getChildren().add(topSide());
        topPane.getChildren().add(modeText);

        centerPane.getChildren().add(buzzwordGrid.getPane());

        bottomPane.getChildren().add(levelControls.getPane());

        rightPane.getChildren().add(timer.getPane());
        rightPane.getChildren().add(typingField);
        rightPane.getChildren().add(scoreRecord.getPane());
        rightPane.getChildren().add(targetScoreBoard.getPane());


        groupOne.getChildren().add(leftPane);

        groupTwo.getChildren().add(topPane);
        groupTwo.getChildren().add(centerPane);
        groupTwo.getChildren().add(bottomPane);

        groupThree.getChildren().add(rightPane);

        this.getChildren().addAll(groupOne, groupTwo, groupThree);
    }





    private VBox topSide(){
        VBox vb = new VBox();
        Text title = new Text("BUZZWORD");
        vb.getChildren().addAll(title);

        title.setStyle(
                "-fx-font-size:60; " +
                        "-fx-font-family: Arial; " +
                        "-fx-fill: rgba(0, 100, 100, 0.5);" +
                        "-fx-fill: linear-gradient(to right, rgba(0, 100, 100, 0.2), rgba(0, 100, 100, 1));"
        );
        vb.setStyle("-fx-spacing:20; -fx-alignment:center;");
        return vb;
    }

    private void initStyle(){
        this.setStyle("-fx-spacing:100;");
        groupTwo.setStyle("-fx-spacing:40");

        groupTwo.setAlignment(Pos.TOP_CENTER);

        rightPane.setStyle("-fx-spacing:20; -fx-alignment:center;");
        topPane.setStyle("-fx-spacing:20; -fx-alignment:center;");

        modeText.setText("NULL MODE");
        modeText.setStyle(
                "-fx-font-size:20; " +
                        "-fx-font-family: Arial; " +
                        "-fx-underline: true;"+
                        "-fx-fill: rgba(20, 20, 20, 0.5);"
        );
    }


    private class LeftSpacing{
        //private double size = 120;
        private double size = 80;
        private Pane space = new Pane();

        public LeftSpacing(){
            space.setMinSize(size, 1);
        }

        public Pane getPane(){
            return space;
        }

        public double getSize() {
            return size;
        }

        public void setSize(double size) {
            this.size = size;
        }
    }

}
