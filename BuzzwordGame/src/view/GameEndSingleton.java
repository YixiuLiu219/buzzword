package view;

import behavior.Function;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.util.Collection;

/**
 * Created by Yixiu Liu on 12/7/2016.
 */
public class GameEndSingleton extends Stage {
    private static GameEndSingleton singleton = new GameEndSingleton();

    private Scene dialogScene;
    private ScrollPane wordContainer;
    private VBox wordPane;
    private Text titleText;
    private Text wordContainerLabel;
    private VBox container;

    private Button back;
    private Button next;

    private boolean isInitilialized = false;

    private GameEndSingleton() {

    }

    public static GameEndSingleton getSingleton() {
        if (singleton == null)
            singleton = new GameEndSingleton();
        return singleton;
    }

    public void init(Stage primaryStage) {
        isInitilialized = true;

        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        this.centerOnScreen();

        //init nodes
        container = new VBox();
        titleText = new Text();
        wordContainerLabel = new Text();
        wordContainer = new ScrollPane();
        wordPane = new VBox();
        back = new Button("BACK TO LEVEL SELECT");
        next = new Button("NEXT LEVEL");
        dialogScene = new Scene(container);

        //add
        container.getChildren().addAll(titleText, wordContainerLabel, wordContainer, back, next);
        wordContainer.setContent(wordPane);

        titleText.setStyle("-fx-font-size:24; " +
                "-fx-font-family: Arial; " +
                "-fx-fill: rgba(255, 255, 255, 0.8);");
        wordContainerLabel.setStyle("-fx-font-size:16; " +
                "-fx-font-family: Arial; " +
                "-fx-fill: rgba(0, 100, 100, 0.5);");

        wordContainer.setPrefViewportWidth(200);
        wordContainer.setPrefViewportHeight(200);
        wordContainer.setStyle("-fx-background:rgba(0,50,50,0.5)");

        wordPane.minWidthProperty().bind(wordContainer.prefViewportWidthProperty());
        wordPane.minHeightProperty().bind(wordContainer.prefViewportHeightProperty());
        wordPane.setStyle("-fx-alignment: top-center;");

        back.setStyle("-fx-base:gray;");
        next.setStyle("-fx-base:gray;");

        container.setAlignment(Pos.CENTER);
        container.setPrefHeight(400);
        container.setPrefWidth(400);
        container.setStyle("-fx-padding: 10px; -fx-spacing:20px; -fx-background-color:rgba(50,50,50,0.8); ");


        dialogScene.setFill(Color.TRANSPARENT);
        this.initStyle(StageStyle.TRANSPARENT);
        this.setScene(dialogScene);
    }

    public boolean isInitilialized(){
        return isInitilialized;
    }

    public void show(String title, Collection<String> allWords, Function backFunction, Function nextFunction) {
        titleText.setText(title);

        wordPane.getChildren().clear();
        for(String s: allWords){
            wordPane.getChildren().add(newText(s));
        }

        back.setOnAction(e->{
            backFunction.execute();
            this.hide();
        });

        next.setOnAction(e->{
            nextFunction.execute();
            this.hide();
        });

        dialogScene.setOnKeyPressed(e->{
            if(e.isControlDown()){
                if(e.getCode() == KeyCode.RIGHT) {
                    nextFunction.execute();
                    this.hide();
                }
            }
        });

        showAndWait();
    }

    public void show(String title, Collection<String> allWords, Function backFunction) {
        next.setDisable(true);
        show(title, allWords, backFunction, ()->{});
        next.setDisable(false);
    }

    private Text newText(String text){
        Text newText = new Text(text);
        newText.setStyle("-fx-fill:WHITE");
        return newText;
    }
}