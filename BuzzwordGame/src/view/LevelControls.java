package view;

import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Created by Yixiu Liu on 12/6/2016.
 */
public class LevelControls {
    private String levelTitle = "NULL LEVEL";
    private String buttonText = "PAUSE";
    private String restartText = "RESTART";

    private VBox container = new VBox();
    private Text levelText = new Text();
    private Button playPause = new Button();
    private Button restartButton = new Button();

    public LevelControls(){
        init();
        initStyle();
    }

    private void init() {
        container.getChildren().addAll(levelText, playPause, restartButton);
    }

    private void initStyle() {
        levelText.setText(levelTitle);
        playPause.setText(buttonText);
        restartButton.setText(restartText);

        container.setStyle("-fx-spacing: 10; -fx-alignment:center; -fx-padding:10");
        playPause.setStyle("-fx-base:rgba(100,100,100, 0.5);");
        levelText.setStyle("-fx-font-size: 30; -fx-fill:WHITE");
        restartButton.setStyle("-fx-base:rgba(100,100,100, 0.5);");
    }

    public VBox getPane(){
        return container;
    }

    public Text getLevelText() {
        return levelText;
    }

    public Button getPlayPause() {
        return playPause;
    }

    public Button getRestart() { return restartButton; }
}

